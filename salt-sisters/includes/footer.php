<div id="footer">

   

    <!-- Begin contacts //-->

    <div class="column-contacts">

        <h3>Cibaria International</h3>

        <ul>

            <li class="phone">(+951) 823 8490</li>

            <li class="email"><a href="http://www.cibariastoresupply.com/shop/contact/" rel="nofollow"><span class="__cf_email__" data-cfemail="60031513140f0d05121305121609030520030902011209014d090e140c4e030f0d">[email&#160;protected]</span><script data-cfhash='f9e31' type="text/javascript">/* <![CDATA[ */!function(t,e,r,n,c,a,p){try{t=document.currentScript||function(){for(t=document.getElementsByTagName('script'),e=t.length;e--;)if(t[e].getAttribute('data-cfhash'))return t[e]}();if(t&&(c=t.previousSibling)){p=t.parentNode;if(a=c.getAttribute('data-cfemail')){for(e='',r='0x'+a.substr(0,2)|0,n=2;a.length-n;n+=2)e+='%'+('0'+('0x'+a.substr(n,2)^r).toString(16)).slice(-2);p.replaceChild(document.createTextNode(decodeURIComponent(e)),c)}p.removeChild(t)}}catch(u){}}()/* ]]> */</script></a></li>

        </ul>

    </div>

    <!-- End contacts //-->



    <!-- Begin footer columns //-->

    <div class="column4">

        <h3>Cibaria Stores</h3>

        <ul>

            <li><a href="http://www.cibariasoapsupply.com/shop" title="Wholesale Soap Making Oils">Wholesale Soap Making Oils</a></li>

            <li><a href="http://www.cibaria-intl.com" title="Wholesale Olive Oils">Wholesale Olive Oils</a></li>

        </ul>

    </div>

    <div class="column3">

        <h3>My Account</h3>

        <ul>

            <li><a href="http://www.cibariastoresupply.com/shop/customer/account/" rel="nofollow" title="My Account">My Account</a></li>

            <li><a href="http://www.cibariastoresupply.com/shop/sales/order/history/" rel="nofollow" title="Order History">Order History</a></li>

        </ul>

    </div>

    <div class="column2">

        <h3>Customer Service</h3>

        <ul>

            <li><a href="http://www.cibariastoresupply.com/shop/contact/" rel="nofollow" title="Contact Us">Contact Us</a></li>
            <li><a href="http://www.cibariastoresupply.com/shop/credit-card-payments/" title="Credit Card Payments">Credit Card Payments</a></li>

            <li><a href="http://www.cibariastoresupply.com/shop/freight-policy" rel="nofollow" title="Freight Policy">Freight Policy</a></li>

            <li><a href="http://www.cibariastoresupply.com/shop/payments" rel="nofollow" title="Website Payment Procedure">Website Payment Procedure</a></li>

            <li><a href="http://www.cibariastoresupply.com/shop/300-minimum" rel="nofollow" title="Minimum Order">Minimum Order</a></li>

            <li><a href="http://www.cibariastoresupply.com/shop/ordering-policy" rel="nofollow" title="Ordering Policy">Ordering Policy</a></li>

            <li><a href="http://www.cibariastoresupply.com/shop/damage-policy" rel="nofollow" title="Damage Policy">Damage Policy</a></li>

            <li><a href="http://www.cibariastoresupply.com/shop/policy-changes" rel="nofollow" title="Policy Changes">Policy Changes</a></li>

            <li><a href="http://www.cibariastoresupply.com/shop/privacy-policy" rel="nofollow" title="Privacy Policy">Privacy Policy</a></li>

            <li><a href="http://www.cibariastoresupply.com/shop/site-terms" rel="nofollow" title="Terms and Conditions">Terms & Conditions</a></li>

        </ul>

    </div>

    <div class="column1">

        <h3>Information</h3>

        <ul>

            <li><a href="http://www.cibariastoresupply.com/shop/about-us" title="About Us">About Us</a></li>

            <li><a href="http://www.cibariastoresupply.com/shop/faq" title="FAQ">FAQ</a></li>

            <li><a href="http://www.cibariastoresupply.com/shop/company-holidays" title="Company Holidays">Company Holidays</a></li>

            <li><a href="http://www.cibariastoresupply.com/shop/articles" title="Articles">Articles</a></li>

        </ul>

    </div>



</div>
<div id="footer-mobile">

    <div class="footer-menu-mobile">

        <h3>Cibaria Stores</h3>



        <div class="footer-menu-mobile-nav">

            <li><a href="http://cibariasoapsupply.com/shop" title="Wholesale Soap Making Oils">Wholesale Soap Making Oils</a></li>

            <li><a href="http://cibaria-intl.com" title="Wholesale Olive Oils">Wholesale Olive Oils</a></li>

        </div>

        <h3>My Account</h3>



        <div class="footer-menu-mobile-nav">

            <li><a href="http://www.cibariastoresupply.com/shop/customer/account/" rel="nofollow">My Account</a></li>

            <li><a href="http://www.cibariastoresupply.com/shop/sales/order/history/" rel="nofollow">Order History</a></li>

        </div>

        <h3>Customer Service</h3>



        <div class="footer-menu-mobile-nav">

            <li><a href="http://www.cibariastoresupply.com/shop/contact/" rel="nofollow">Contact Us</a></li>

            <li><a href="http://www.cibariastoresupply.com/shop/credit-card-payments" rel="nofollow">Credit Card Payments</a></li>
            <li><a href="http://www.cibariastoresupply.com/shop/freight-policy" rel="nofollow">Freight Policy</a></li>

            <li><a href="http://www.cibariastoresupply.com/shop/payments" rel="nofollow">Website Payment Procedure</a></li>

            <li><a href="http://www.cibariastoresupply.com/shop/300-minimum" rel="nofollow">Minimum Order</a></li>

            <li><a href="http://www.cibariastoresupply.com/shop/ordering-policy" rel="nofollow">Ordering Policy</a></li>

            <li><a href="http://www.cibariastoresupply.com/shop/damage-policy" rel="nofollow">Damage Policy</a></li>

            <li><a href="http://www.cibariastoresupply.com/shop/policy-changes" rel="nofollow">Policy Changes</a></li>

            <li><a href="http://www.cibariastoresupply.com/shop/privacy-policy" rel="nofollow">Privacy Policy</a></li>

            <li><a href="http://www.cibariastoresupply.com/shop/site-terms" rel="nofollow">Terms & Conditions</a></li>

        </div>

        <h3>Information</h3>



        <div class="footer-menu-mobile-nav">

            <li><a href="http://www.cibariastoresupply.com/shop/about-us" title="About Us">About Us</a></li>

            <li><a href="http://www.cibariastoresupply.com/shop/faq" title="FAQ">FAQ</a></li>

            <li><a href="http://www.cibariastoresupply.com/shop/company-holidays" title="Company Holidays">Company Holidays</a></li>

            <li><a href="http://www.cibariastoresupply.com/shop/articles" title="Articles">Articles</a></li>

        </div>

    </div>

</div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-30465466-2', 'cibariastoresupply.com');
  ga('send', 'pageview');

</script>

<script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","licenseKey":"1f87743da2","applicationID":"1703103","transactionName":"YVFUZxQHWBFTB0xdX1gbd1ASD1kMHQdZQFFaW1EcBQdCB1ULSk0fQF1TRA==","queueTime":0,"applicationTime":452,"atts":"TRZXEVwdSx8=","errorBeacon":"bam.nr-data.net","agent":""}</script></body>
</html>