    
<meta charset="UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<title>Salt Sisters</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="s.a.l.t. sisters Products are fresh, unrefined, non-GMO, gluten-free without any artificial fillers or MSG " />
<meta name="robots" content="INDEX,FOLLOW" />


<link rel="icon" href="http://www.cibariastoresupply.com/shop/skin/frontend/default/cibaria-supply/favicon.ico" type="image/x-icon" />

<link rel="shortcut icon" href="http://www.cibariastoresupply.com/shop/skin/frontend/default/cibaria-supply/favicon.ico" type="image/x-icon" />

<script type="text/javascript">

//<![CDATA[

    var BLANK_URL = 'http://www.cibariastoresupply.com/shop/js/blank.html';

    var BLANK_IMG = 'http://www.cibariastoresupply.com/shop/js/spacer.gif';

//]]>

</script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script type="text/javascript" src="http://www.cibariastoresupply.com/shop/skin/frontend/default/cibaria-supply/js/menu-mobile.js"></script>
<link rel="stylesheet" type="text/css" href="http://www.cibariastoresupply.com/shop/skin/frontend/default/cibaria-supply/css/stylesheet.css" media="all" />
<link rel="stylesheet" type="text/css" href="http://www.cibariastoresupply.com/shop/skin/frontend/default/cibaria-supply/css/nav.css" media="all" />
<link rel="stylesheet" type="text/css" href="http://www.cibariastoresupply.com/shop/skin/frontend/default/cibaria-supply/css/mobile.css" media="all" />
<link rel="stylesheet" type="text/css" href="http://www.cibariastoresupply.com/shop/skin/frontend/default/cibaria-supply/css/cloud-zoom.css" media="all" />
<link rel="stylesheet" type="text/css" href="http://www.cibariastoresupply.com/shop/skin/frontend/default/cibaria-supply/css/slideshow.css" media="all" />
<link rel="stylesheet" type="text/css" href="http://www.cibariastoresupply.com/shop/skin/frontend/default/cibaria-supply/css/carousel.css" media="all" />
<link rel="stylesheet" type="text/css" href="http://www.cibariastoresupply.com/shop/skin/frontend/default/cibaria-supply/css/print.css" media="print" />
<script type="text/javascript" src="http://www.cibariastoresupply.com/shop/js/prototype/prototype.js"></script>
<script type="text/javascript" src="http://www.cibariastoresupply.com/shop/js/prototype/validation.js"></script>
<script type="text/javascript" src="http://www.cibariastoresupply.com/shop/js/scriptaculous/builder.js"></script>
<script type="text/javascript" src="http://www.cibariastoresupply.com/shop/js/scriptaculous/effects.js"></script>
<script type="text/javascript" src="http://www.cibariastoresupply.com/shop/js/scriptaculous/dragdrop.js"></script>
<script type="text/javascript" src="http://www.cibariastoresupply.com/shop/js/scriptaculous/controls.js"></script>
<script type="text/javascript" src="http://www.cibariastoresupply.com/shop/js/scriptaculous/slider.js"></script>
<script type="text/javascript" src="http://www.cibariastoresupply.com/shop/js/varien/js.js"></script>
<script type="text/javascript" src="http://www.cibariastoresupply.com/shop/js/varien/form.js"></script>
<script type="text/javascript" src="http://www.cibariastoresupply.com/shop/js/varien/menu.js"></script>
<script type="text/javascript" src="http://www.cibariastoresupply.com/shop/js/mage/translate.js"></script>
<script type="text/javascript" src="http://www.cibariastoresupply.com/shop/js/varien/weee.js"></script>
<link href="http://www.cibariastoresupply.com/shop/rss/catalog/category/cid/231/store_id/3/" title="Salt Sisters RSS Feed" rel="alternate" type="application/rss+xml" />
<!--[if lt IE 8]>
<link rel="stylesheet" type="text/css" href="http://www.cibariastoresupply.com/shop/skin/frontend/default/cibaria-supply/css/iestyles.css" media="all" />
<![endif]-->
<!--[if lt IE 7]>
<link rel="stylesheet" type="text/css" href="http://www.cibariastoresupply.com/shop/skin/frontend/default/cibaria-supply/css/ie7minus.css" media="all" />
<script type="text/javascript" src="http://www.cibariastoresupply.com/shop/js/lib/ds-sleight.js"></script>
<script type="text/javascript" src="http://www.cibariastoresupply.com/shop/js/varien/iehover-fix.js"></script>
<![endif]-->

<script type="text/javascript">var Translator = new Translate([]);</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-30465466-2', 'auto');
  ga('send', 'pageview');

</script>

    <style type="text/css">
    input.button-product1-page {
    background-color: #E6348A;
    border: none;
    padding: 6px 20px;
    font-size: 13px;
    color: #fff;
    text-decoration: none;
    cursor: pointer;
    line-height: 17px;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    -khtml-border-radius: 3px;
    border-radius: 3px;
}
</style>