<?php

//functions
$functions_path = $_SERVER['DOCUMENT_ROOT'];
$functions_path .= '/shop/includes/functions.php';
require($functions_path);

//get from recipes
$recipe_title = $_REQUEST['recipe_title'];
$front_image = $_REQUEST['front_image'];
$back_image = $_REQUEST['back_image'];

//surpress data null warning
error_reporting(E_ERROR | E_PARSE);
?>

<html dir="ltr" lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Recipes - Cibaria International</title>
    <?php include(url_path('/shop/includes/head.php')); ?>
    <link rel="stylesheet" href="css/recipes.css"/>
</head>
<body>
<script type="text/javascript">
    jQuery.noConflict();
    var $j = jQuery;
</script>
<?php include(url_path('/shop/includes/google-analytics.php')); ?>
<?php include(url_path('/shop/includes/header.php')); ?>
<div id="container-wrapper">
    <div id="container">
        <div class="breadcrumb"></div>
        <div class="category-top-content"> <h1 style="text-align: center;"><?php echo $recipe_title; ?></h1></div>

<!--recipe_post start-->
<div style="width:100%; height:auto;">
	<div style="max-width: 800px; padding: 15px; margin:auto;">
       <?php if(isMobilePhone() || isIE()) : ?>
               <img src="<?php echo showRecipeImage($front_image); ?>" alt="<?php echo $recipe_title; ?>" style="width:100%; height:auto"/>
        <?php else : ?>
           <a href="<?php echo showRecipeImage($front_image); ?>" download="<?php echo $recipe_title; ?>-front-recipe.jpg" target="_blank">
               <img src="<?php echo showRecipeImage($front_image); ?>" alt="<?php echo $recipe_title; ?>" style="width:100%; height:auto;"/></a>
        <?php endif; ?>
<div style="text-align:center; padding: 10px;margin:auto;">
    <?php if(isMobilePhone() || isIE()) : ?>
        <form action="recipes-view.php" method="post">
            <input type="hidden" name="front_image" value="<?php echo showRecipeImage($front_image); ?>"/>
            <input type="hidden" name="recipe_title" value="<?php echo $recipe_title; ?>"/>
            <input type="image" src="images/download.jpg" alt="Submit" border="0"/>
        </form>
        <hr>
    <?php else : ?>
  <a href="<?php echo showRecipeImage($front_image); ?>" download="<?php echo $recipe_title; ?>-front-recipe.jpg" target="_blank">
    <img src="images/download.jpg" alt="Download <?php echo $recipe_title; ?>"></a>
    <hr>
    <?php endif; ?>
</div>
	</div>
	<div style="max-width: 800px; padding: 15px; margin:auto;">
        <?php if(isMobilePhone() || isIE()) : ?>
            <a href="<?php echo showRecipeImage($back_image); ?>" target="_blank">
                <img src="<?php echo showRecipeImage($back_image); ?>" alt="<?php echo $recipe_title; ?>" style="width:100%; height:auto;"/>
            </a>
        <?php else : ?>
            <a href="<?php echo showRecipeImage($back_image); ?>" download="<?php echo $recipe_title; ?>-back-recipe.jpg" target="_blank">
                <img src="<?php echo showRecipeImage($back_image); ?>" alt="<?php echo $recipe_title; ?>" style="width:100%; height:auto;"/></a>
        <?php endif; ?>
<div style="text-align:center; padding: 10px; margin:auto;">
    <?php if(isMobilePhone() || isIE()) : ?>
        <form action="recipes-view.php" method="post">
            <input type="hidden" name="back_image" value="<?php echo getRecipeImageUrl($back_image); ?>"/>
            <input type="hidden" name="recipe_title" value="<?php echo $recipe_title; ?>"/>
            <input type="image" src="images/download.jpg" alt="Submit" border="0"/>
        </form>
        <hr>
    <?php else : ?>
        <a href="<?php echo showRecipeImage($back_image); ?>" download="<?php echo $recipe_title; ?>-back-recipe.jpg" target="_blank">
            <img src="images/download.jpg" alt="Download <?php echo $recipe_title; ?>"></a>
        <hr>
    <?php endif; ?>
</div>
</div>
</div>
        <!--end-->
<?php include(url_path('/shop/includes/footer.php')); ?>

    </div>
</div>

</body>
</html>