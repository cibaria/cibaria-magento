<?php
/**
 * Created by PhpStorm.
 * User: bryansiegel
 * Date: 5/15/18
 * Time: 10:59 AM
 */

$categories = array(
    'appetizers' => 'Appetizers',
    'deserts' => 'Deserts ',
    'dips' => 'Dips',
    'drinks' => 'Drinks',
    'fish' => 'Fish',
    'fruit' => 'Fruit',
    'main_courses' => 'Main Courses',
    'meat' => 'Meat',
    'pasts' => 'Pastas',
    'salad' => 'Salad',
    'sandwiches' => 'Sandwiches',
    'sides' => 'Sides',
    'soups' => 'Soups',
);

$product_categories = array(
    'arbequina' => 'Arbequina',
    'arbosana' => 'Arbosana',
    'balsamic_vinegar_of_modena' => 'Balsamic Vinegar of Modena',
    'carolea' => 'Carolea',
    'coratina' => 'Coratina',
    'dipping_oils' => 'Dipping Oils',
    'extra_virgin_olive_oil' => 'Extra Virgin Olive Oil',
    'flavored_balsamic_vinegar' => 'Flavored Balsamic Vinegar',
    'flavored_extra_virgin_olive_oils' => 'Flavored Extra Virgin Olive Oils',
    'flavored_white_balsamic_vinegar' => 'Flavored White Balsamic Vinegar',
    'frantoio' => 'Frantoio',
    'fused_olive_oils' => 'Fused Olive Oils',
    'hojiblanca' => 'Hojiblanca',
    'honey_serrano_chili_vinegar' => 'Honey Serrano Chili Vinegar',
    'infused_olive_oils' => 'Infused Olive Oils',
    'kalamata' => 'Kalamata',
    'koroneiki' => 'Koroneiki',
    'leccino' => 'Leccino',
    'mission' => 'Mission',
    'ogliarola' => 'Ogliarola',
    'pantry_products' => 'Pantry Products',
    'picual' => 'Picual',
    'sevillano' => 'Sevillano',
    'specialty_oils' => 'Specialty Oils',
    'umbrian' => 'Umbrian ',
    'varietal_blends' => 'Varietal Blends',
    'white_balsamic_vinegar_of_modena' => 'White Balsamic Vinegar of Modena',
);
// sort($product_categories);