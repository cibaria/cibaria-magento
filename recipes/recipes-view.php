<?php

//functions
$functions_path = $_SERVER['DOCUMENT_ROOT'];
$functions_path .= '/shop/includes/functions.php';
require($functions_path);

//get from recipes post
$front_image = $_REQUEST['front_image'];
$back_image = $_REQUEST['back_image'];
$recipe_title = $_REQUEST['recipe_title'];

//surpress data null warning
error_reporting(E_ERROR | E_PARSE);
?>
<html dir="ltr" lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Recipes - Cibaria International</title>
    <?php include(url_path('/shop/includes/head.php')); ?>
    <link rel="stylesheet" href="css/recipes.css"/>
</head>
<body>
<script type="text/javascript">
    jQuery.noConflict();
    var $j = jQuery;
</script>
<?php include(url_path('/shop/includes/google-analytics.php')); ?>
<?php include(url_path('/shop/includes/header.php')); ?>
<div id="container-wrapper">
    <div id="container">
        <div class="breadcrumb"></div>
        <div class="category-top-content"> <h1 style="text-align: center;"><?php echo $recipe_title; ?></h1></div>
<?php if(!empty($front_image)) : ?>
<div style="max-width: 800px; padding: 15px; margin:auto;">
    <img src="<?php echo $front_image; ?>" alt="Front Image" style="width:100%; height:auto"/>
    </div>
<?php endif; ?>
<?php if(!empty($back_image)) : ?>
<div style="max-width: 800px; padding: 15px; margin:auto;">
    <img src="<?php echo $back_image; ?>" alt="Back Image" style="width:100%; height:auto"/>
    </div>
<?php endif; ?>

        <?php include(url_path('/shop/includes/footer.php')); ?>
    </div>
</div>
</body>
</html>