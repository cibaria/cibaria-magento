<?php
$functions_path = $_SERVER['DOCUMENT_ROOT'];
$functions_path .= '/shop/includes/functions.php';

require($functions_path);
include_once('categories.php');

?>

<html dir="ltr" lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Recipes - Cibaria International</title>
    <?php include(url_path('/shop/includes/head.php')); ?>
    <link rel="stylesheet" href="<?php echo Mage::getBaseUrl(); ?>css/recipes.css"/>
</head>

<body>
<script type="text/javascript">
    jQuery.noConflict();
    var $j = jQuery;
</script>

<?php include(url_path('/shop/includes/google-analytics.php')); ?>
<?php include(url_path('/shop/includes/header.php')); ?>
<div id="container-wrapper">
    <div id="container">
        <div class="breadcrumb"></div>
        <div class="category-top-content"><h1 style="text-align:center;">Recipe Cards</h1></div>
        <div id="column-right">
            <div id="box-category-area">
                <div class="box">
                    <div class="box-heading">
                        <span class="category">Food Categories</span>
                    </div>
                    <div class="box-content" id="listId">
                        <ul class="category-list">
                            <?php foreach ($categories as $category) { ?>
                                <li><input type="checkbox" name="category-products" class="category-products" id="<?php echo $category; ?>"><?php echo $category; ?></input></li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                <!-- end -->
                <div class="box">
                    <div class="box-heading">
                        <span class="account">Product Categories</span>
                    </div>
                    <div class="box-content" id="listId">
                        <ul class="category-list">
                            <?php foreach ($product_categories as $key => $value) { ?>
                                <li><input type="checkbox" name="category-products" class="category-products" id="<?php echo $key; ?>"><?php echo $value; ?></input></li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div id="content"><?php include('recipes.php'); ?></div>
        <?php //include(url_path('/shop/includes/footer.php')); ?>
    </div>
</div>


<script>
//    $j('#listing-type-list').easyPaginate({
//        paginateElement: 'div.box-product',
//        elementsPerPage: 3,
//        effect: 'climb'
//    });

    //show/hide div if checkbox is checked
    $j(function () {
        $j(":checkbox").change(function () {
            //get the category name from id
            var category = $j(this).attr('id');
            console.log(category);
            //if the category is checked and matches the id show or hid the id of the category in the product list
            if (this.id === category && this["name"] === "category-products" && this.checked) {
                $j('.product').hide();
                $j('div.' + category).show();

            } else {
                $j('.product').show();
                $j('div.' + category).show();
            }

        });
    });
</script>
</body>
</html>