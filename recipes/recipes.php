<div class="listing-type-list catalog-listing" id="listing-type-list">
    <div class="listing-item">
        <br/>
        <div id="box-list2">
        <div class="box-product">
            <?php foreach($recipes as $recipe) : ?>
                <?php if($recipe['activated'] == 'yes') : ?>
            <div class="new-product product <?php foreach($recipe['categories'] as $category) { echo $category['category'] . " "; } ?><?php foreach($recipe['product_categories'] as $product) { echo $product['product_category']. " "; } ?>">
                <div class="">
                <div class="boxgrid">
                    <div class="image">
                        <?php
                        //surpress data null warning
                        error_reporting(E_ERROR | E_PARSE);
                        ?>
                        <img src="<?php echo showRecipeImage($recipe['hero_image']); ?>" alt="<?php echo $recipe['title']; ?>" width="150px" height="125px">
                    </div>
                </div>
                <div class="boxgrid-bottom">
                    <div class="name"><strong><?php echo $recipe['title']; ?></strong></div>
                    <div class="price">
                        <div>
                            <form action="recipes_post.php" method="post">
                                <input type="hidden" name="id" value="<?php echo $recipe['id']; ?>"/>
                                <input type="hidden" name="front_image" value="<?php echo $recipe['front_image']; ?>"/>
                                <input type="hidden" name="recipe_title" value="<?php echo $recipe['title']; ?>"/>
                                <input type="hidden" name="back_image" value="<?php echo $recipe['back_image']; ?>"/>
                                <input type="image" src="<?php echo Mage::getBaseUrl(); ?>images/recipes/download.jpg" alt="Submit" border="0"/>
                            </form>
                       </div>
                    </div>
                    <br>
                </div>
            </div>
            </div>
        <?php endif ?>
        <?php endforeach ?>
            <!-- end -->
        </div>
        <br/>
    </div>
    </div>
</div>