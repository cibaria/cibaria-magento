<?php
$functions_path = $_SERVER['DOCUMENT_ROOT'];
$functions_path .= '/includes/functions.php';

require($functions_path);
?>

<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
    <meta charset="UTF-8"/>
    <title>Request An Account</title>
    <?php include(url_path('/includes/head.php')); ?>
    <script type="text/javascript">
        //jquery no conflict with prototype
        jQuery.noConflict();
        var $j = jQuery;

        $j(document).ready(function () {
            // validate signup form on keyup and submit
            $j("#requestaccountForm").validate({
                rules: {
                    firstname: "required",
                    lastname: "required",

                    email: {
                        required: true,
                        email: true
                    }
                },
                messages: {
                    firstname: "Please enter your First Name",
                    lastname: "Please enter your Last Name",
                    email: "Please enter a valid email address"
                }
            });
        });
    </script>
</head>
<body>
<?php include(url_path('/includes/header.php')); ?>
<div id="container-wrapper">
    <div id="container">
        <div id="content">
            <form name="contactform" id="requestaccountForm"
                  action="<?php echo Mage::getBaseUrl(); ?>request-account/contact_post.php"
                  method="post">
                <h1 style="border-bottom:1px dotted #dddddd;">Request A Wholesale Account</h1>
                <p>
                    Thank you for visiting our website. In order to <b>view pricing you must have a seller account</b>.
                    We do not approve accounts on the weekends or during off hours. Cibaria International is open
                    Monday through Friday from 7:30 am PST to 4:00 pm PST.
                </p>
                <h1 style="border-bottom:1px dotted #dddddd;"">Contact Us</h1>
                <p><strong>Telephone:</strong> (951) 823-8490 | <strong>Toll Free:</strong> (800) 988-OILS | <strong>Fax:</strong> (951) 823-8495</p>
                <p><strong>Email:</strong> <a href="mailto:customerservice@cibaria-intl.com">customerservice@cibaria-intl.com</a> or <a href="<?php echo Mage::getBaseUrl(); ?>contact/">Contact Form</a></p>
                <div style="background-color:#f6f7e9;border: 1px solid #dddddd;padding:5px;">
                    <h1 style="border-bottom:1px dotted #dddddd;">Please Fill In The Information</h1>
                    <div style="width:80%;">
                        <fieldset class="group-select" style="border:none;">
                            <div style="float:left">
                                <h3>Business Information</h3>
                                <ul>
                                    <li>
                                        <div class="input-box">
                                            <label for="firstname">First Name <span class="required">(Required)</span></label>
                                            <br/>
                                            <input type="text" id="firstname" name="firstname" class="required-entry input-text"/>
                                        </div>
                                        <div class="input-box">
                                            <label for="lastname">Last Name <span class="required">(Required)</span></label>
                                            <br/>
                                            <input type="text" id="lastname" name="lastname" class="required-entry input-text"/>
                                        </div>
                                        <div class="input-box">
                                            <label for="company">Company Name</label>
                                            <br/>
                                            <input type="text" name="company" class="input-text" />
                                        </div>
                                        <div class="input-box">
                                            <label for="telephone">Telephone</label>
                                            <br/>
                                            <input type="text" name="telephone" class="input-text"/>
                                        </div>
                                        <div class="input-box">
                                            <label for="email">Email <span class="required">(Required)</span></label>
                                            <br/>
                                            <input type="text" id="email" name="email" class="required-entry input-text validate-email"/>
                                        </div>
                            </div>
                            <div style="float:left; margin-left:50px;">
                                <h3>Business Address</h3>
                                <div class="input-box">
                                    <label for "address">Street Address</label>
                                    <br/>
                                    <input type="text" name="address" class="input-text"/>
                                </div>
                                <div class="input-box">
                                    <label for "address">City</label>
                                    <br/>
                                    <input type="text" name="city" class="input-text"/>
                                </div>
                                <div class="input-box">
                                    <label for "address">State</label>
                                    <br/>
                                    <input type="text" name="state" class="input-text"/>
                                </div>
                                <div class="input-box">
                                    <label for "address">Zipcode</label>
                                    <br/>
                                    <input type="text" name="zipcode" class="input-text"/>
                                </div>
                                <div class="input-box">
                                    <label for "country">Country</label>
                                    <br/>
                                    <input type="text" name="country" class="input-text"/>
                                </div>

                            </div>
                            <div style="float:left;margin-left:50px;">
                                <h3>Information</h3>
                                <div class="input-box">
                                    <label for="store_website">Store Website</label>
                                    <br/>
                                    <input type="text" name="store_website" class="input-text"/>
                                </div>
                                <div class="input-box">
                                    <label for="tax_id">Tax ID</label>
                                    <br/>
                                    <input type="text" name="tax_id" class="input-text"/>
                                </div>
                                <div class="input-box">
                                    <label for="open_date">Estimated Open Date</label>
                                    <br/>
                                    <input type="text" name="open_date" class="input-text"/></div>
                                <div class="input-box">
                                    <label for="find_us">How Did You Find Us?</label>
                                    <br/>
                                    <input type="text" name="find_us" class="input-text"/>
                                </div>

                                <div class="clear"></div>
                            </div>
                    </div>

                    <div style="padding-left:25px;">
                        <div class="input-box">
                            <label for="message"><b>Questions/Comments</b></label>
                            <br/>
                            <textarea name="message" class="input-text" cols="5" rows="5"></textarea>
                        </div>
                        <div class="input-box">
                            <label for="message"><b>Looking for something you didn't find on our website? Let us know.</b></label>
                            <br/>
                            <textarea name="larger_quantities" class="input-text" cols="5" rows="5"></textarea>
                        </div>
                    </div>
                    <div class="button-set" style="padding-left:25px;padding-bottom:15px;">
                        <input type="submit" class="form-button" style="background-color:purple;color:white;padding-top:5px;padding-bottom:5px;padding-left:10px; padding-right:10px;border-color:purple;border-radius: 5px;"/>
                    </div>
            </form>
        </div>
        <?php include(url_path('/includes/footer.php')); ?>
    </div>
</div>
</body>
</html>