<?php

$functions_path = $_SERVER['DOCUMENT_ROOT'];
$functions_path .= '/includes/functions.php';

require($functions_path);
 
if(isset($_POST['email'])) {

    // EDIT THE 2 LINES BELOW AS REQUIRED
 
    // $email_to = "bsiegel@cibaria-intl.com";
//    $email_to = "cibaria_karenmoore@hotmail.com";
    // $email_to = "rghazarian@cibaria-intl.com";
     $email_to = "bsiegel@cibaria-intl.com";
    $email_subject = "Cibaria Store Supply New Account Request";
 
     
 
     
 
    function died($error) {
 
        // your error code can go here

        include(url_path('/includes/request-account/contact-error-before.php'));
 
        echo "<h1 style='color:white;'>We are very sorry, but there were error(s) found with the form you submitted.</h1>";
 
        echo "These errors appear below.<br /><br />";
 
        echo $error."<br /><br />";
 
        echo "Please go back and fix these errors.<br /><br />";

        include(url_path('/includes/request-account/contact-error-after.php'));
 
        die();
 
    }
 
     
 
    // validation expected data exists
 
    if(!isset($_POST['firstname']) ||
 
        !isset($_POST['lastname']) ||

        !isset($_POST['address']) ||

        !isset($_POST['company']) ||

        !isset($_POST['store_website']) ||

        !isset($_POST['tax_id']) ||

        !isset($_POST['open_date']) ||

        !isset($_POST['find_us']) ||
 
        !isset($_POST['email']) ||
 
        !isset($_POST['telephone']) ||

        !isset($_POST['state']) ||

        !isset($_POST['city']) ||

        !isset($_POST['country']) ||

        !isset($_POST['larger_quantities']) ||

        !isset($_POST['zipcode']) ||

        !isset($_POST['message'])) {
 
        died('We are sorry, but there appears to be a problem with the form you submitted.');       
 
    }
 
     
 
    $first_name = $_POST['firstname']; // required
 
    $last_name = $_POST['lastname']; // required
 
    $telephone = $_POST['telephone']; // not required

    $company = $_POST['company']; // not required
   
    $email_from = $_POST['email']; // required
   
    $address = $_POST['address']; // required
 
    $store_website = $_POST['store_website']; // required
 
    $tax_id = $_POST['tax_id']; // required

    $open_date = $_POST['open_date']; // required

    $find_us = $_POST['find_us']; // required
    
    $zipcode = $_POST['zipcode']; // required
    
    $city = $_POST['city']; // required

    $state = $_POST['state']; // not required

    $country = $_POST['country']; // not required

    $message = $_POST['message']; // not required

    $larger_quantities = $_POST['larger_quantities']; // not required

     
 
    $error_message = "";
 
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
 
  if(!preg_match($email_exp,$email_from)) {
 
    $error_message .= 'The Email Address you entered does not appear to be valid.<br />';
 
  }
 
    $string_exp = "/^[A-Za-z .'-]+$/";
 
  if(!preg_match($string_exp,$first_name)) {
 
    $error_message .= 'The First Name you entered does not appear to be valid.<br />';
 
  }
 
  if(!preg_match($string_exp,$last_name)) {
 
    $error_message .= 'The Last Name you entered does not appear to be valid.<br />';
 
  }
 
  // if(strlen($message) < 2) {
 
  //   $error_message .= 'The Comments you entered do not appear to be valid.<br />';
 
  // }
 
  if(strlen($error_message) > 0) {
 
    died($error_message);
 
  }
 
    $email_message = "Form details below.\n\n";
 
     
 
    function clean_string($string) {
 
      $bad = array("content-type","bcc:","to:","cc:","href");
 
      return str_replace($bad,"",$string);
 
    }
 
     
 
    $email_message .= "First Name: ".clean_string($first_name)."\n";
 
    $email_message .= "Last Name: ".clean_string($last_name)."\n";

    $email_message .= "Company: ".clean_string($company)."\n";

    $email_message .= "Telephone: ".clean_string($telephone)."\n";

    $email_message .= "Email: ".clean_string($email_from)."\n";

    $email_message .= "Street: ".clean_string($address)."\n";

    $email_message .= "City: ".clean_string($city)."\n";
    
    $email_message .= "State: ".clean_string($state)."\n";

    $email_message .= "Zipcode: ".clean_string($zipcode)."\n";

    $email_message .= "Country: ".clean_string($country)."\n";

    $email_message .= "Store Website: ".clean_string($store_website)."\n";

    $email_message .= "Tax ID: ".clean_string($tax_id)."\n";

    $email_message .= "Estimated Open Date: ".clean_string($open_date)."\n";

    $email_message .= "Looking for something you didn't find on our website: ".clean_string($larger_quantities)."\n";

    $email_message .= "Where did you find us: ".clean_string($find_us)."\n";

    $email_message .= "Message: ".clean_string($message)."\n";
 
     
 
// create email headers
 
$headers = 'From: customerservice@cibaria-intl.com'."\r\n".
 
'Reply-To: '.$email_from."\r\n" .
 
'X-Mailer: PHP/' . phpversion();
 
@mail($email_to, $email_subject, $email_message, $headers);  
 
?>
    <!-- include your own success html here -->
 <?php include(url_path('/includes/request-account/contact-html.php')); ?>





 
<?php
 
}
 
?>