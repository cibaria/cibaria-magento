<?php require_once('../../app/Mage.php') ?><!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
    <meta charset="UTF-8"/>
    <title>Request An Account</title>    <?php include('../includes/head.php'); ?></head>
<body><!--jquery no conflict-->
<script>
  (function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,"script","//www.google-analytics.com/analytics.js","ga");

  ga("create", "UA-30465466-2", "auto");
  ga("send", "pageview");

</script>
<!--end--><!--header--><?php include('../includes/header.php'); ?>
<div id="container-wrapper">
    <div id="container">
        <div id="content">

            <form name="contactform" action="/shop/support-request/support_request_post.php" method="post"> <h1>Support Request</h1>

                <div id="contact-form"><p>If you need your password reset please click  <a href="http://www.cibariastoresupply.com/shop/password-request/">here</a>. For all other inquiries please fill out your information below so that a member of our support team can assist you.</p>
                </div>
                <fieldset class="group-select"><h4 class="legend">Request An Account</h4>
                    <ul>
                        <li>
                            <div class="input-box"><label for="first_name">First Name <span
                                        class="required">*</span></label><br/> <input type="text" name="first_name"
                                                                                      class="required-entry input-text"/>
                            </div>
                            <div class="input-box"><label for="last_name">Last Name <span
                                        class="required">*</span></label><br/> <input type="text" name="last_name"
                                                                                      class="required-entry input-text"/>
                            </div>
                            <div class="input-box"><label for="telephone">Telephone</label><br/> <input type="text"
                                                                                                        name="telephone"
                                                                                                        class="input-text"/>    
                            <div class="input-box"><label for="company_name">Company Name</label><br/> <input type="text"
                                                                                                        name="company_name"
                                                                                                        class="input-text"/>
                            </div>
                              <div class="input-box"><label for="email">Email <span class="required">*</span></label><br/>
                                <input type="text" name="email" class="required-entry input-text validate-email"/></div>

                            <div class="clear"></div>
                            <div class="input-box"><label for="message">Message</label><br/> <textarea name="message"
                                                                                                       class="required-entry input-text"
                                                                                                       style="height:250px;width:450px;"
                                                                                                       cols="5"
                                                                                                       rows="5"></textarea>
                            </div>
                        </li>
                    </ul>
                    <div class="button-set"><p class="required">* Required Fields</p>                            <input
                            type="submit" class="form-button"/>
                        <!--        <input type="text" name="hideit" id="hideit" value="" style="display:none !important;" />-->
                        <!--        <button class="form-button" type="submit"><span>--><!--</span></button>-->
                    </div>
            </form>
            </fieldset>                <!-- end content -->            </div>
    </div>
</div>            <?php include_once '../includes/footer.php'; ?></body>
</html>