<?php
$functions_path = $_SERVER['DOCUMENT_ROOT'];
$functions_path .= '/shop/includes/functions.php';
require($functions_path);
?>

    <!DOCTYPE html>
    <html dir="ltr" lang="en">
    <head>
        <meta charset="UTF-8"/>
        <title>QA/Statements</title>
        <?php include(url_path('/shop/includes/head.php')); ?>
        <link rel="stylesheet" href="css/style.css"/>

    </head>
    <body>

    <?php include(url_path('/shop/includes/header.php')); ?>
    <div id="container-wrapper">
        <div id="container">
            <div id="content">
                <h1 style="font-size:30px; text-align:center;padding-top:10px;">Quality Assurance Statements</h1>
                <br/>
            </div>
            <div class="listing-type-list catalog-listing">
                <div class="listing-item">
                    <br/>

                    <div class="box-product">
                        <div>
                            <div class="boxgrid">
                                <div class="image">
                                    <img src="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/small/siliker-2018.png" alt="Audit Recognition"/>
                                </div>
                            </div>
                            <div class="boxgrid-bottom">
                                <div class="name">
                                    <strong>Audit Recognition</strong></div>
                                <div class="price">
                                    <a href="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/siliker-2018.png" download="images/audit-2018.png"
                                       rel="nofollow"><img src="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/download.jpg" alt="thumb"/></a>
                                </div>
                                <br>
                            </div>
                        </div>
                        <div>
                            <div class="boxgrid">
                                <div class="image"><a href="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/certificate-of-guarantee.jpg"
                                                      title="Certification of Guarantee"><br/><br/><br/><img
                                            src="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/small/certification-of-guarantee-2017-sm2.jpg"
                                            alt="Certification of Guarantee"></a></div>
                            </div>
                            <div class="boxgrid-bottom">
                                <div class="name"><a href="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/certification-of-guarantee-large.jpg"
                                                     title="Certification of Guarantee"><strong>Certification of
                                            Guarantee</strong></a></div>
                                <div class="price">
                                    <a href="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/certification-of-guarantee-large.jpg"
                                       download="images/certification-of-guarantee-large.jpg" rel="nofollow"><img
                                            src="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/download.jpg" alt="thumb"/></a>
                                </div>
                                <br>
                            </div>
                        </div>
                        <div>
                            <div class="boxgrid">
                                <div class="image"><a href="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/continueing-product-guarantee-2018-large.jpg"
                                                      title="Continuing Production Guarantee"><br/><br/><br/><img
                                            src="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/small/continueing-product-guarantee-2018-sm.jpg"
                                            alt="Continuing Production Guarantee"/></a></div>
                            </div>
                            <div class="boxgrid-bottom">
                                <div class="name"><a href="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/continueing-product-guarantee-2018-large.jpg"
                                                     title="Continuing Production Guarantee"><strong>Continuing
                                            Production Guarantee</strong></a></div>
                                <div class="price">
                                    <a href="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/continueing-product-guarantee-2018-large.jpg"
                                       download="images/continueing-product-guarantee-2018-large.jpg"
                                       rel="nofollow"><img src="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/download.jpg" alt="thumb"/></a>
</div>
                                <br>
                            </div>
                        </div>
                        <div>
                            <div class="boxgrid">
                                <div class="image"><a href="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/Processed Food Registration.pdf"
                                                      title="Department of Public Health"><br/><br/><br/><img
                                            src="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/small/Processed Food Registration small.jpg"
                                            alt="Department of Public Health"/></a></div>
                            </div>
                            <div class="boxgrid-bottom">
                                <div class="name"><a href="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/Processed-Food-Registration2.jpg"
                                                     title="Department of Public Health"><strong>Department of Public
                                            Health</strong></a></div>
                                <div class="price">
                                    <a href="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/dept-of-health-2018.pdf" download="dept-of-health-2018.pdf"
                                       rel="nofollow"><img src="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/download.jpg" alt="thumb"/></a>
                                    </div>
                                <br>

                            </div>
                        </div>
                        <div>
                            <div class="boxgrid">
                                <div class="image"><a href="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/gluten_free_statement.jpg"
                                                      title="Gluten Free Statement"><br/><br/><br/><img
                                            src="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/small/gluten_free_statement.jpg"
                                            alt="Gluten Free Statement"/></a></div>
                            </div>
                            <div class="boxgrid-bottom">
                                <div class="name"><a href="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/gluten_free_statement.jpg"
                                                     title="Gluten Free Statement"><strong>Gluten Free
                                            Statement</strong></a></div>
                                <div class="price">
                                    <a href="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/gluten_free_statement.jpg"
                                       download="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/gluten_free_statement.jpg" rel="nofollow"><img
                                            src="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/download.jpg" alt="thumb"/></a>
                                    </div>
                                <br>

                            </div>
                        </div>

                        <div>
                            <div class="boxgrid">
                                <div class="image"><a href="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/prop65_guidelines.pdf"
                                                      title="Prop 65 Guidelines"><br/><br/><br/><img
                                            src="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/small/prop65_guidelines.jpg" alt="Prop 65 Guidelines" /></a></div>
                            </div>
                            <div class="boxgrid-bottom">
                                <div class="name"><a href="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/prop65_guidelines.pdf"
                                                     title="Prop 65 Guidelines"><strong>Prop 65 Guidelines</strong></a>
                                </div>
                                <div class="price">
                                    <a href="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/prop65_guidelines.pdf" download="images/prop65_guidelines.pdf"
                                       rel="nofollow"><img src="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/download.jpg" alt="thumb"/></a>
                                    </div>
                                <br>

                            </div>
                        </div>

                        <div>
                            <div class="boxgrid">
                                <div class="image"><a href="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/org-cert-2018.jpg"
                                                      title="Organic Product Certification"><br/><br/><br/><img
                                            src="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/small/org-cert-2018-sm.jpg" alt="Organic Product Certification" /></a></div>
                            </div>
                            <div class="boxgrid-bottom">
                                <div class="name"><a href="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/org-cert-2018.jpg"
                                                     title="Organic Product Certification"><strong>Organic Product
                                            Certification</strong></a></div>
                                <div class="price">
                                    <a href="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/org-cert-2018.jpg"
                                       download="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/org-cert-2018.jpg" rel="nofollow"><img
                                            src="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/download.jpg" alt="thumb"/></a>
                                    </div>
                                <br>

                            </div>
                        </div>
                        <div>
                            <div class="boxgrid">
                                <div class="image"><a href="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/view/specialty-food-association-2017-large.jpg"
                                                      title="Specialty Food Association"><br/><br/><br/><img
                                            src="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/small/specialty-food-association-2017-sm.jpg" alt="Specialty Food Association" /></a></div>
                            </div>
                            <div class="boxgrid-bottom">
                                <div class="name"><a href="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/view/specialty-food-association-2017-large.jpg"
                                                     title="Specialty Food Association"><strong>Specialty Food
                                            Association</strong></a></div>
                                <div class="price">
                                    <a href="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/view/specialty-food-association-2017-large.jpg"
                                       download="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/view/specialty-food-association-2017-large.jpg"
                                       rel="nofollow"><img src="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/download.jpg" alt="thumb"/></a>
                                    </div>
                                <br>

                            </div>
                        </div>
                        <div>
                            <div class="boxgrid">
                                <div class="image"><a href="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/Organic Processed Product Registration.pdf"
                                                      title="Organic Processed Product Registration"><br/><br/><br/><img
                                            src="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/small/Organic Processed Product Registration small.jpg" alt="Organic Processed Product Registration" /></a></div>
                            </div>
                            <div class="boxgrid-bottom">
                                <div class="name"><a href="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/Organic Processed Product Registration.pdf"
                                                     title="Organic Processed Product Registration"><strong>Organic
                                            Processed Product Registration</strong></a></div>
                                <div class="price">
                                    <a href="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/Organic Processed Product Registration.pdf"
                                       download="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/Organic Processed Product Registration.pdf" rel="nofollow"><img
                                            src="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/download.jpg" alt="thumb"/></a>
                                    </div>
                                <br>

                            </div>
                        </div>
                        <div>
                            <div class="boxgrid">
                                <div class="image"><a href="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/glass-bottle-statement.pdf"
                                                      title="Glass Bottle Statement"><br/><br/><br/><img
                                            src="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/small/glass-bottle-statement.jpg" alt="Glass Bottle Statement" /></a></div>
                            </div>
                            <div class="boxgrid-bottom">
                                <div class="name"><a href="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/glass-bottle-statement.pdf"
                                                     title="Glass Bottle Statement"><strong>Glass Bottle
                                            Statement</strong></a></div>
                                <div class="price">
                                    <a href="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/glass-bottle-statement.pdf"
                                       download="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/glass-bottle-statement.pdf" rel="nofollow"><img
                                            src="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/download.jpg" alt="thumb"/></a>
                                    </div>
                                <br>
                            </div>
                        </div>
                        <div>
                            <div class="boxgrid">
                                <div class="image"><a href="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/gmo-status-of-flavors-2017.jpg"
                                                      title="GMO Status of Flavors"><br/><br/><br/><img
                                            src="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/gmo-status.jpg" alt="GMO Status of Flavors" /></a></div>
                            </div>
                            <div class="boxgrid-bottom">
                                <div class="name"><a href="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/gmo-status-of-flavors-2017.jpg"
                                                     title="GMO Status of Flavors"><strong>GMO Status of
                                            Flavors</strong></a></div>
                                <div class="price">
                                    <a href="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/gmo-status-of-flavors-2017.jpg"
                                       download="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/gmo-status-of-flavors-2017.jpg" rel="nofollow"><img
                                            src="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/download.jpg" alt="thumb"/></a>
                                    </div>
                                <br>

                            </div>
                        </div>
                        <div>
                            <div class="boxgrid">
                                <div class="image"><a href="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/certificate-of-authenticity.pdf"
                                                      title="Certificate Of Authenticity"><br/><br/><br/><img
                                            src="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/certificate-of-authenticity.png" alt="Certificate Of Authenticity" height="200px"
                                            width="200px"/></a></div>
                            </div>
                            <div class="boxgrid-bottom">
                                <div class="name"><a href="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/certificate-of-authenticity.pdf"
                                                     title="Certificate Of Authenticity"><strong>Certificate Of
                                            Authenticity</strong></a></div>
                                <div class="price">
                                    <a href="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/certificate-of-authenticity.pdf"
                                       download="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/certificate-of-authenticity.pdf" rel="nofollow"><img
                                            src="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/download.jpg" alt="thumb"/></a>
                                    </div>
                                <br>

                            </div>
                        </div>
                        <div>
                            <div class="boxgrid">
                                <div class="image"><a href="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/WBENC Certificate 2017.pdf"
                                                      title="WBENC Certification"><br/><br/><br/><img
                                            src="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/wbenc-sm-2016.jpg" alt="WBENC Certification" height="200px"
                                            width="200px"/></a></div>
                            </div>
                            <div class="boxgrid-bottom">
                                <div class="name"><a href="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/WBENC Certificate 2017.pdf"
                                                     title="WBENC Certification"><strong>WBENC
                                            Certification</strong></a></div>
                                <div class="price">

                                    <a href="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/WBENC Certificate 2017.pdf"
                                       download="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/WBENC Certificate 2017.pdf" rel="nofollow"><img
                                            src="<?php echo Mage::getBaseUrl(); ?>images/qa-statements/download.jpg" alt="thumb"/></a>
                                    </div>
                                <br>

                            </div>
                        </div>
                    </div>
                    <br/>
                </div>
            </div>
            <?php include(url_path('/shop/includes/footer.php')); ?>
        </div>
    </div>
    </body>
    </html>