<?php
$functions_path = $_SERVER['DOCUMENT_ROOT'];
$functions_path .= '/shop/includes/functions.php';

require($functions_path);

?>
    <?php if ($session->isLoggedIn()) { ?>
    <!DOCTYPE html>
    <html dir="ltr" lang="en">

    <head>
        <meta charset="UTF-8" />
        <title>Olive Oil Nutrition Labels - Cibaria International</title>
        <?php include(url_path('/shop/includes/head.php')); ?>
        <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css"/>
        <script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    </head>

    <body>
     <?php include(url_path('/shop/includes/google-analytics.php')); ?>
        <?php include(url_path('/shop/includes/header.php')); ?>
        <div id="container-wrapper">
            <div id="container">
                <div id="content">
                    <h1 style="font-size:30px; text-align:center;padding-top:10px;">Nutrition Labels</h1>
                    <table class="attribute" id="nutrition">
                      <thead>
                        <tr>
                          <td></td>
                          <td>Name</td>
                          <td></td>
                        </tr>
                      </thead>
                      <tbody>
                    <?php include(url_path('/shop/includes/nutrition-labels/_oils.php')); ?>
                    <?php include(url_path('/shop/includes/nutrition-labels/_vinegars.php')); ?>
                    <?php include(url_path('/shop/includes/nutrition-labels/_25_star_white.php')); ?>
                    <?php include(url_path('/shop/includes/nutrition-labels/_25_star_dark.php')); ?>
                    <?php include(url_path('/shop/includes/nutrition-labels/_pantry_items.php')); ?>
                    <?php include(url_path('/shop/includes/nutrition-labels/_specialty_items.php')); ?>
                    </tbody>
                  </table>
                    <?php include(url_path('/shop/includes/footer.php')); ?>
                    <script type="text/javascript">
                          jQuery.noConflict();
                          var $j = jQuery;
                          $j(document).ready( function () {
                          $j('#nutrition').DataTable();
                      } );
                    </script>
                </div>
            </div>
        </div>
    </body>

    </html>
    <?php } else {  echo "<script type='text/javascript'>window.location.assign('/shop/customer/account/login/')</script>"; } ?>