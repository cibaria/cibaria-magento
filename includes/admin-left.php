<table class="attribute">
    <thead>
    <tr>
        <td>
           <strong>My Account Information</strong>
        </td>
    </tr>
    </thead>
    <tr>
        <td>
            <a href="<?php echo Mage::getBaseUrl() . "customer/account/"; ?>">Account Dashboard</a>
        </td>
        </tr>
    <tr>
        <td>
            <a href="<?php echo Mage::getBaseUrl() . "customer/account/edit/"; ?>">Account Information</a>
        </td>
        </tr>
    <tr>
        <td>
            <a href="<?php echo Mage::getBaseUrl() . "customer/address/"; ?>">Address Book</a>
        </td>
        </tr>
    <tr>
        <td>
            <a href="<?php echo Mage::getBaseUrl() . "sales/order/history/"; ?>">My Orders</a>
        </td>
        </tr>
    <tr>
        <td>
            <a href="<?php echo Mage::getBaseUrl() . "wishlist/"; ?>">My Wishlist</a>
        </td>
    </tr>
</table>