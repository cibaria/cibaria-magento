<html dir="ltr" lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Request Account - Thank You - Cibaria International</title>
    <?php include(url_path('/includes/head.php')); ?>
</head>

<body>

<?php include(url_path('/includes/google-analytics.php')); ?>
<?php include(url_path('/includes/header.php')); ?>
<div id="container-wrapper">
    <div id="container">
        <div class="breadcrumb"></div>
        <div class="category-top-content"><h1 style="text-align:center;">Request Account Submission</h1></div>

        <div id="content" style="text-align: center; background-color:red; color:white; border-radius: 5px; padding-top: 15px;">