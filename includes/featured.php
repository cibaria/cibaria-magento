      <div class="box">
            <div class="box-heading"><span class="featured">Featured</span></div>

        </div>

<div class="box">
            <div class="box-content">
                <div class="box-product">
                    <?php
$featured = Mage::getModel('catalog/category')->load(223); // YOU NEED TO CHANGE 5 TO THE ID OF YOUR CATEGORY

$featured = $featured->getProductCollection();
foreach ($featured->getAllIds() as $featured_product) {
	$_product = Mage::getModel('catalog/product')->load($featured_product);?>


<?php

	try {
		$image_url = $_product->getSmallImage();
	} catch (Exception $e) {
		$image_url = Mage::getDesign()->getSkinUrl('images/catalog/product/placeholder/image.jpg', array('_area' => 'frontend'));
	}

	?>

                    <div>
                        <!-- Begin boxgrid //-->
                        <div class="boxgrid">
                            <div class="image"><a href="<?php echo $_product->getProductUrl();?>" title="<?php echo $_product->getName();?>"><img src="<?php echo "https://cibariasoapsupply.com/shop/media/catalog/product" . $image_url;?>" alt="<?php echo $_product->getName();?>" height="140px"/></a></div>
                        </div>
                        <!-- End boxgrid //-->

                        <!-- Begin boxgrid bottom //-->
                        <div class="boxgrid-bottom">
                            <div class="name"><a href="<?php echo $_product->getProductUrl();?>" title="<?php echo $_product->getName();?>"><?php echo $_product->getName();?></a></div>
                        </div>
                        <!-- End boxgrid bottom //-->

                    </div>
                    <?php }?>
                </div>
            </div>
        </div>