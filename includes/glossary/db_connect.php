<?php
/*
 * The local code is within the main cibaria project (all sites main Magento project)
 * The hosted file is located within the cibariastore site (the cibariastoresupply.com design)
 */
require('db_connect_info.php');
global $db;
/* ----- Connection Strings for Local and remote ------ */
//remote
//$db = mysqli_connect('localhost', 'root', 'LbUHDX6VUajoDxt', 'glossary');
//local
$db = mysqli_connect(SERVER, USERNAME, PASSWORD, DATABASE);
/*-----------------------------------------------------*/

if(!$db){
    die('Could Not Connect To The Database');
}

/*
 * Queries
 */
//query the database for everything
function all($db){

$query_all = mysqli_query($db, "SELECT * FROM definitions ORDER by title ASC");
if($query_all == true) {
while($row = mysqli_fetch_row($query_all)){ ?>
    <?php if ($row[1] == 'A.O.C.') { ?>
        <div style="border-bottom: solid 1px #B063C3; margin-bottom: 15px; ">
            <h2 style="color:#B063C3; margin-bottom:-5px;"><a id="A" style="color:#B063C3;">A</a></h2>
        </div>
    <?php } elseif ($row[1] == 'Bitter') { ?>
        <div style="border-bottom: solid 1px #FF8500; margin-bottom: 15px; margin-top:40px; ">
            <h2 style="color:#FF8500; margin-bottom:-5px;"><a id="B" style="color:#FF8500;">B</a></h2>
        </div>
    <?php } elseif ($row[1] == 'Calorie') { ?>
        <div style="border-bottom: solid 1px #B063C3; margin-bottom: 15px; margin-top:40px; ">
            <h2 style="color:#B063C3; margin-bottom:-5px;"><a id="C" style="color:#B063C3;">C</a></h2>
        </div>
    <?php } elseif ($row[1] == 'DAGs Test/Score') { ?>
        <div style="border-bottom: solid 1px #FF8500; margin-bottom: 15px; margin-top:40px; ">
            <h2 style="color:#FF8500; margin-bottom:-5px;"><a id="D" style="color:#FF8500;">D</a></h2>
        </div>
    <?php } elseif ($row[1] == 'Earthy') { ?>
        <div style="border-bottom: solid 1px #B063C3; margin-bottom: 15px; margin-top:40px; ">
            <h2 style="color:#B063C3; margin-bottom:-5px;"><a id="E" style="color:#B063C3;">E</a></h2>
        </div>
    <?php } elseif ($row[1] == 'Fat') { ?>
        <div style="border-bottom: solid 1px #FF8500; margin-bottom: 15px; margin-top:40px; ">
            <h2 style="color:#FF8500; margin-bottom:-5px;"><a id="F" style="color:#FF8500;">F</a></h2>
        </div>
    <?php } elseif ($row[1] == 'Genetically Modified Organism (GMO)') { ?>
        <div style="border-bottom: solid 1px #B063C3; margin-bottom: 15px; margin-top:40px; ">
            <h2 style="color:#B063C3; margin-bottom:-5px;"><a id="G" style="color:#B063C3;">G</a></h2>
        </div>
    <?php } elseif ($row[1] == 'Hand Picked') { ?>
        <div style="border-bottom: solid 1px #FF8500; margin-bottom: 15px; margin-top:40px; ">
            <h2 style="color:#FF8500; margin-bottom:-5px;"><a id="H" style="color:#FF8500;">H</a></h2>
        </div>
    <?php } elseif ($row[1] == 'Interesterification') { ?>
        <div style="border-bottom: solid 1px #B063C3; margin-bottom: 15px; margin-top:40px; ">
            <h2 style="color:#B063C3; margin-bottom:-5px;"><a id="I" style="color:#B063C3;">I</a></h2>
        </div>
    <?php } elseif ($row[1] == 'Kosher') { ?>
        <div style="border-bottom: solid 1px #FF8500; margin-bottom: 15px; margin-top:40px; ">
            <h2 style="color:#FF8500; margin-bottom:-5px;"><a id="K" style="color:#FF8500;">K</a></h2>
        </div>
    <?php } elseif ($row[1] == 'Lipids') { ?>
        <div style="border-bottom: solid 1px #B063C3; margin-bottom: 15px; margin-top:40px; ">
            <h2 style="color:#B063C3; margin-bottom:-5px;"><a id="L" style="color:#B063C3;">L</a></h2>
        </div>
    <?php } elseif ($row[1] == 'Median of Defects') { ?>
        <div style="border-bottom: solid 1px #FF8500; margin-bottom: 15px; margin-top:40px; ">
            <h2 style="color:#FF8500; margin-bottom:-5px;"><a id="M" style="color:#FF8500;">M</a></h2>
        </div>
    <?php } elseif ($row[1] == 'Oleic Acid') { ?>
        <div style="border-bottom: solid 1px #B063C3; margin-bottom: 15px; margin-top:40px; ">
            <h2 style="color:#B063C3; margin-bottom:-5px;"><a id="O" style="color:#B063C3;">O</a></h2>
        </div>
    <?php } elseif ($row[1] == 'Peppery') { ?>
        <div style="border-bottom: solid 1px #FF8500; margin-bottom: 15px; margin-top:40px; ">
            <h2 style="color:#FF8500; margin-bottom:-5px;"><a id="P" style="color:#FF8500;">P</a></h2>
        </div>
    <?php } elseif ($row[1] == 'Rancid') { ?>
        <div style="border-bottom: solid 1px #B063C3; margin-bottom: 15px; margin-top:40px; ">
            <h2 style="color:#B063C3; margin-bottom:-5px;"><a id="R" style="color:#B063C3;">R</a></h2>
        </div>
    <?php } elseif ($row[1] == 'Satured Fatty Acids') { ?>
        <div style="border-bottom: solid 1px #FF8500; margin-bottom: 15px; margin-top:40px; ">
            <h2 style="color:#FF8500; margin-bottom:-5px;"><a id="S" style="color:#FF8500;">S</a></h2>
        </div>
    <?php } elseif ($row[1] == 'Trans Fat (Trans Fatty Acids)') { ?>
        <div style="border-bottom: solid 1px #B063C3; margin-bottom: 15px; margin-top:40px; ">
            <h2 style="color:#B063C3; margin-bottom:-5px;"><a id="T" style="color:#B063C3;">T</a></h2>
        </div>
    <?php } elseif ($row[1] == 'Vegetable water') { ?>
        <div style="border-bottom: solid 1px #FF8500; margin-bottom: 15px; margin-top:40px; ">
            <h2 style="color:#FF8500; margin-bottom:-5px;"><a id="V" style="color:#FF8500;">V</a></h2>
        </div>
    <?php } elseif ($row[1] == 'Wax Content') { ?>
        <div style="border-bottom: solid 1px #B063C3; margin-bottom: 15px; margin-top:40px; ">
            <h2 style="color:#B063C3; margin-bottom:-5px;"><a id="W" style="color:#B063C3;">W</a></h2>
        </div>


    <?php } ?>

    <div style='margin-bottom:15px; border-bottom: 1px dotted grey;'>
        <h3 style='margin-bottom: 1px;'>
            <?php echo $row[1]; ?>
        </h3>
        <?php echo $row[2]; ?>
        <br/></div>
<?php } } else { ?>
    <h1>No Results</h1>
<?php } } ?>

<?php
/* =========================== This Function works to grab everything with php 5.3 not 5.2 ========================*/
// function get_all($query){
//     foreach($query as $result){
//         echo "Title:" . $result['title'] . "<br />";
//         echo "Description: " . $result['description'] . "<br /><hr />";
//     }
// }
/* ================================================================================================================*/
//the name is kinda self explanitory but just incase it inserts the values from new to the db
function insert($db, $title, $description){
    $insert_into = mysqli_query($db, "INSERT INTO definitions (id, title, description) VALUES('', '$title','$description')");
}

function delete($db, $id){
    mysqli_query($db, "DELETE FROM definitions WHERE id = '$id'")
    or die(mysql_error());
    echo "<script type='text/javascript'>window.location.assign('index.php')</script>";

}



