<?php
//db connection
//magento file includes

require('../includes/admin/db_connect.php');
require('../includes/admin/magenot.php');

/*
 * Connection Settings
 *
 */
//local
$db = mysqli_connect(SERVER, USERNAME, PASSWORD, DATABASE);
//remote
//$db = mysqli_connect('localhost', 'root', 'LbUHDX6VUajoDxt', 'glossary');

?>

<?php if ($session->isLoggedIn()) { ?>
    <?php include('../design/admin/header.php'); ?>
    <?php include('../design/admin/body.php'); ?>
    <div class="content-header">
        <table cellspacing="0">
            <tr>
                <td style="width:50%;"><h3 class="icon-head head-products">Manage Glossary Terms</h3></td>
                <td class="a-right">
                    <button  id="" type="button" class="scalable add" onclick="setLocation('new.php')" style=""><span>Add Glossary Term</span></button></td>
            </tr>
        </table>
    </div>

    <div id="productGrid">
        <table cellspacing="0" class="actions">
            <tr>
            </tr>
        </table>

        <div class="grid">
            <div class="hor-scroll">
                <table cellspacing="0" class="data" id="productGrid_table">

                    <thead>
                    <tr class="headings">
                        <th width="2"><span class="nobr">&nbsp;</span></th>
                        <th width="20"><span class="nobr"><a href="#" name="entity_id" title="asc" class="not-sort"><span class="sort-title">ID</span></a></span></th>
                        <th width="30"><span class="nobr"><a href="#" name="name" title="asc" class="not-sort"><span class="sort-title">Title</span></a></span></th>
                        <th width="15"><span class="nobr"><a href="#" name="name" title="asc" class="not-sort"><span class="sort-title">Description</span></a></span></th>
                        <th width="5"><span class="nobr"><a href="#" name="name" title="asc" class="not-sort"><span class="sort-title">Edit</span></a></span></th>

                    </tr>
                    </thead>
                    <tbody>

                    <?php
                    //values
                    get_all(); ?>

                    </tbody>

                </table>
            </div>
        </div>
    </div>
    <?php include('../design/admin/footer.php'); ?>
<?php
} else {
    echo "<script type='text/javascript'>window.location.assign('/shop/index.php/admin/')</script>";

} ?>




