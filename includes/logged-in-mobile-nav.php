<div id="menu-mobile-wrapper">
    <div id="menu-mobile" style="background-color:black;">
        <div id="menu-mobile-nav">
            <ul>
                <li><a href="http://www.cibariastoresupply.com">Home</a></li>
                <li><a href="<?php echo Mage::getBaseUrl() . "olive-oils.html"; ?>">Olive Oils</a>
                <li><a href="<?php echo Mage::getBaseUrl() . "wholesale-vinegars.html"; ?>">Vinegars</a>
                <li><a href="<?php echo Mage::getBaseUrl() . "specialty-oils.html"; ?>">Specialty</a>
                <li><a href="<?php echo Mage::getBaseUrl() . "organic-oils.html"; ?>">Organic</a>
                <li><a href="<?php echo Mage::getBaseUrl() . "pantry-oils.html"; ?>">Pantry Items</a>
                <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/bottles.html"; ?>">Bottles</a>
                <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/fustis.html"; ?>">Fustis</a>
                <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories.html"; ?>">Accessories</a>
                <li><a href="<?php echo Mage::getBaseUrl() . "customer/account/logout/"; ?>">Log Out</a></li>
        </div>
        <div id="search" style="float:right">
            <div class="button-search"></div>
            <form id="search_mini_form" action="<?php echo Mage::getBaseUrl() . "catalogsearch/result/";?>" rel="nofollow" method="get">
                <input id="search" type="text" class="input-text" name="q" value="" placeholder="Search">
            </form>
        </div>
        <div class="shopping_cart_mobile">
            <a href="<?php echo Mage::getBaseUrl() . "checkout/cart/"; ?>">Shoppping Cart</a>
        </div>
    </div>
</div>