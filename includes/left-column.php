<div id="column-left">

    <div id="box-category-area">


        <?php
        //Categories
//        $category = Mage::getSingleton('catalog/layer')->getCurrentCategory();
        $category = Mage::getModel('catalog/category')->load(261);
        $categories = $category->getCollection()
            ->addAttributeToSelect(array('name', 'thumbnail'))
            ->addAttributeToFilter('is_active', 1)
            ->addAttributeToSort('name', 'ASC')
            ->addIdFilter($category->getChildren());
        ?>
        <div class="box">
            <?php
            //hide category if it's empty. yes the semi collin stops the foreach after the first loop
            foreach($categories as $categoryname); ?>
            <?php if(isset($categoryname) && $categoryname > "0") { ?>
                <div class="box-heading"><span class="category">Categories</span></div>
            <?php }else {
                echo "";
            } ?>
            <div class="box-content">
                <div class="box-category">
                    <ul>

                        <?php foreach($categories as $index) { ?>

                            <li><a href="<?php echo $index->getUrl(); ?>" title="<?php echo $index->getName(); ?>"><?php echo $index->getName(); ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="box">
        <div class="box-heading"><span class="account">Back In Stock</span></div>
        <div class="box-content">
            <div class="box-product">
                <?php
                $featured = Mage::getModel('catalog/category')->load(225);  // YOU NEED TO CHANGE 5 TO THE ID OF YOUR CATEGORY
                $featured = $featured->getProductCollection();
                foreach($featured->getAllIds() as $featured_product)
                {
                    $_product = Mage::getModel('catalog/product')->load($featured_product);


                    ?>

                    <div>
                        <!-- Begin boxgrid //-->
                        <div class="boxgrid">
                            <div class="image"><a href="<?php echo $_product->getProductUrl();?>"><img src="<?php echo "http://cibariasoapsupply.com/shop/media/catalog/product" . $_product->getSmallImage(); ?>" alt="<?php echo $_product->getName(); ?>" width="75px" height="50px"/   /></a></div>
                            <div class="box-product-info">
                                <div class="description">
                                    <?php echo $_product->getShortDescription(); ?></div>
                                <div class="more"><a href="<?php echo Mage::getBaseUrl() . $_product->getUrlPath(); ?>" title="<?php echo $_product->getName(); ?>">"></a></div>
                            </div>
                        </div>
                        <!-- End boxgrid //-->

                        <!-- Begin boxgrid bottom //-->
                        <div class="boxgrid-bottom">
                            <div class="name"><a href="<?php echo Mage::getBaseUrl() . $_product->getUrlPath(); ?>" title="<?php echo $_product->getName(); ?>"><?php echo $_product->getName(); ?></a></div>
                        </div>
                        <!-- End boxgrid bottom //-->

                    </div>
                <?php } ?>
            </div>
        </div>
    </div>




    <div class="box">
        <div class="box-heading"><span class="specials">Featured</span></div>
        <div class="box-content">
            <div class="box-product">
                <?php
                $featured = Mage::getModel('catalog/category')->load(223);  // YOU NEED TO CHANGE 5 TO THE ID OF YOUR CATEGORY
                $featured = $featured->getProductCollection();
                foreach($featured->getAllIds() as $featured_product)
                {
                    $_product = Mage::getModel('catalog/product')->load($featured_product); ?>

                    <div>
                        <!-- Begin boxgrid //-->
                        <div class="boxgrid">
                            <div class="image"><a href="<?php echo $_product->getProductUrl();?>"><img src="<?php echo "http://cibariasoapsupply.com/shop/media/catalog/product" . $_product->getSmallImage(); ?>" alt="<?php echo $_product->getName(); ?>" width="75px" height="50px"/   /></a></div>
                            <div class="box-product-info">
                                <div class="description">
                                    <?php echo $_product->getShortDescription(); ?></div>
                                <div class="more"><a href="<?php echo $_product->getProductUrl();?>" title="<?php echo $_product->getName(); ?>">"></a></div>
                            </div>
                        </div>
                        <!-- End boxgrid //-->

                        <!-- Begin boxgrid bottom //-->
                        <div class="boxgrid-bottom">
                            <div class="name"><a href="<?php echo $_product->getProductUrl();?>" title="<?php echo $_product->getName(); ?>"><?php echo $_product->getName(); ?></a></div>
                        </div>
                        <!-- End boxgrid bottom //-->

                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="box">
        <div class="box-heading"><span class="bestsellers">What's New</span></div>
        <div class="box-content">
            <div class="box-product">
                <?php
                $featured = Mage::getModel('catalog/category')->load(224);  // YOU NEED TO CHANGE 5 TO THE ID OF YOUR CATEGORY
                $featured = $featured->getProductCollection();
                foreach($featured->getAllIds() as $featured_product)
                {
                    $_product = Mage::getModel('catalog/product')->load($featured_product); ?>

                    <div>
                        <!-- Begin boxgrid //-->
                        <div class="boxgrid">
                            <div class="image"><a href="<?php echo $_product->getProductUrl();?>"><img src="<?php echo "http://cibariasoapsupply.com/shop/media/catalog/product" .  $_product->getSmallImage(); ?>" alt="<?php echo $_product->getName(); ?>"  width="75px" height="50px"/></a></div>
                            <div class="box-product-info">
                                <div class="description">
                                    <?php echo $_product->getShortDescription(); ?></div>
                                <div class="more"><a href="<?php echo $_product->getProductUrl();?>" title="<?php echo $_product->getName(); ?>">"></a></div>
                            </div>
                        </div>
                        <!-- End boxgrid //-->

                        <!-- Begin boxgrid bottom //-->
                        <div class="boxgrid-bottom">
                            <div class="name"><a href="<?php echo $_product->getProductUrl();?>" title="<?php echo $_product->getName(); ?>"><?php echo $_product->getName(); ?></a></div>
                        </div>
                        <!-- End boxgrid bottom //-->

                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <div id="banner0" class="banner">
        <br class="clear"/>
    </div>
</div>