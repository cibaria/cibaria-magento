<div id="menu-wrapper">
    <ul class="nav">
        <li><a href="http://www.cibariastoresupply.com">Home</a></li>
        <li>
            <a href="<?php echo Mage::getBaseUrl() . "olive-oils.html ";?>" title="Olive Oils">Olive Oils</a>
            <div>
                <div class="nav-column">
                    <h3>Extra Virgin</h3>
                    <ul>
                        <li><a href="<?php echo Mage::getBaseUrl() . "top-sellers ";?>" title="Olive Oil Top Sellers" style="color:#EE8448;">Olive Oil Top Sellers</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oils/extra-virgin.html ";?>" title="Extra Virgin Olive Oils">Extra Virgin Olive Oils</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oils/extra-virgin/australia-evoo.html ";?>" title="Australian Extra Virgin Olive Oil">Australia</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oils/extra-virgin/california-evoo.html ";?>" title="California Extra Virgin Olive Oil">California</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oils/extra-virgin/chile-evoo.html ";?>" title="Chile Extra Virgin Olive Oil">Chile</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oils/extra-virgin/greece-evoo.html ";?>" title="Greece Extra Virgin Olive Oil">Greece</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oils/extra-virgin/italy-evoo.html ";?>" title="Italy Extra Virgin Olive Oil">Italy</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oils/extra-virgin/morocco-evoo.html ";?>" title="Morocco Extra Virgin Olive Oil">Morocco</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oils/extra-virgin/spain-evoo.html ";?>" title="Spain Extra Virgin Olive Oil">Spain</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oils/extra-virgin/tunsia-evoo.html ";?>" title="Tunsia Extra Virgin Olive Oil">Tunsia</a></li>
                    </ul>
                </div>
                <div class="nav-column">
                    <h3>Infused</h3>
                    <ul>
                        <li><a href="<?php echo Mage::getBaseUrl() . "top-sellers ";?>" title="Infused Olive Oil Top Sellers" style="color:#A16FB4">Infused Olive Oil Top Sellers</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oils/infused.html ";?>" title="Infused Olive Oils">Infused Olive Oils</a></li>
                    </ul>
                </div>
                <div class="nav-column">
                    <h3>Flavored EVOO</h3>
                    <ul>
                        <li><a href="<?php echo Mage::getBaseUrl() . "top-sellers ";?>" title="Flavored EVOO Top Sellers" style="color:#AAD55E">Flavored EVOO Top Sellers</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oils/flavored-oils.html ";?>" title="Naturally Flavored Olive Oils">Naturally Flavored Olive Oils</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oils/flavored.html ";?>" title="Flavored Olive Oils">Flavored Olive Oils</a></li>
                    </ul>
                </div>
                <div class="nav-column">
                    <h3>Fused</h3>
                    <ul>
                        <li><a href="<?php echo Mage::getBaseUrl() . "top-sellers ";?>" title="Fused Olive Oils Top Sellers" style="color:pink;">Fused Olive Oil Top Sellers</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oils/fused.html ";?>" title="Fused Olive Oils">Fused Olive Oils</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li>
            <a href="<?php echo Mage::getBaseUrl() . "wholesale-vinegars.html ";?>" title="Wholesale Vinegars">Vinegars</a>
            <div>
                <div class="nav-column">
                    <h3>Modena Balsamics</h3>
                    <ul>
                        <li><a href="<?php echo Mage::getBaseUrl() . "top-sellers ";?>" title="Vinegar Top Sellers" style="color:#EE8448;">Vinegar Top Sellers</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "wholesale-vinegars/modena-balsamics.html ";?>" title="Wholesale Modena Balsamic Vinegars">Modena Balsamic Vinegars</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "wholesale-vinegars/modena-balsamics/25-star-modena-balsamic-vinegar.html ";?>" title="25 Star Modena Balsamic Vinegars">25 Star Modena Balsamic Vinegars</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "wholesale-vinegars/modena-balsamics/8-star-modena-balsamic-vinegar.html ";?>" title="8 Star Modena Balsamic Vinegar">8 Star Modena Balsamic Vinegar</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "wholesale-vinegars/modena-balsamics/6-star-modena-balsamic-vinegar.html ";?>" title="6 Star Modena Balsamic Vinegar">6 Star Modena Balsamic Vinegar</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "wholesale-vinegars/modena-balsamics/4-star-modena-balsamic-vinegar.html ";?>" title="4 Star Modena Balsamic Vinegar">4 Star Modena Balsamic Vinegar</a></li>
                    </ul>
                </div>
                <div class="nav-column">
                    <h3>Flavored</h3>
                    <ul>
                        <li><a href="<?php echo Mage::getBaseUrl() . "top-sellers ";?>" title="Dark Flavored Balsamic Top Sellers" style="color:#A16FB4">Dark Flavored Balsamic Top Sellers</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "top-sellers ";?>" title="White Flavored Balsamic Top Sellers" style="color:#800020">White Flavored Balsamic Top Sellers</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "wholesale-vinegars/flavored-balsamics.html ";?>" title="Flavored Balsamic Vinegars">Flavored Balsamic Vinegars</a></li>
                    </ul>
                </div>
                <div class="nav-column">
                    <h3>Made From Honey</h3>
                    <ul>
                        <li><a href="<?php echo Mage::getBaseUrl() . "wholesale-vinegars/honey.html ";?>" title="Vinegars Made From Honey">Vinegars Made From Honey</a></li>
                    </ul>
                </div>
                <div class="nav-column">
                    <h3>Organic</h3>
                    <ul>
                        <li><a href="<?php echo Mage::getBaseUrl() . "wholesale-vinegars/organic-vinegars.html ";?>" title="Organic Balsamic Vinegars">Organic Balsamic Vinegars</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li><a href="<?php echo Mage::getBaseUrl() . "specialty-oils.html ";?>" title="Specialty Oils">Specialty</a></li>
        <li><a href="<?php echo Mage::getBaseUrl() . "organic-oils.html ";?>" title="Organic Oils">Organic</a></li>
        <li>
            <a href="<?php echo Mage::getBaseUrl() . "pantry-items.html "?>" title="Pantry Items">Pantry Items</a>
            <div>
                <div class="nav-column">
                    <h3>Pantry Items</h3>
                    <ul>
                        <li><a href="<?php echo Mage::getBaseUrl() . "top-sellers ";?>" title="Pantry Item Top Sellers" style="color:#EE8448;">Pantry Item Top Sellers</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "pantry-items.html "?>" title="Pantry Items">Pantry Items</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "pantry-items/barbeque-sauce.html ";?>" title="Barbeque Sauce">Barbeque Sauce</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "pantry-items/dipping-oils.html " ?>" title="Dipping Oils">Dipping Oils</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "pantry-items/olives.html " ?>" title="Olives">Olives</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "top-sellers ";?>" title="Olive Top Sellers" style="color:#A16FB4">Olive Top Sellers</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "pantry-items/spreads.html " ?>" title="Spreads">Spreads</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li>
            <a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories.html ";?>" title="Olive Oil Store Accessories">Accessories</a>
            <div>
                <div class="nav-column">
                    <h3>Fustis</h3>
                    <ul>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/decanters.html ";?>" title="Fustis">Decanters</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/fustis.html ";?>" title="Fustis">Fustis</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/fustis/fusti-stands.html ";?>" title="Fustis Stands">Fustis Stands</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/fustis/fusti-valve.html ";?>" title="Fusti Valve">Fusti Valve</a></li>
                    </ul>
                </div>
                <div class="nav-column">
                    <h3>Bottles</h3>
                    <ul>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/bottles.html ";?>" title="Olive Oil Store Bottles">Bottles</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/bottles/pet.html ";?>" title="PET Bottles">PET</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/bottles/serenade.html ";?>" title="Serenade Bottles">Serenade</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/bottles/stephanie.html ";?>" title="Stephanie Bottles">Stephanie</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/bottles/marasca.html ";?>" title="Marasca Bottles">Marasca</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/bottles/dorica.html ";?>" title="Dorica Bottles">Dorica</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/bottles/bordelese.html ";?>" title="Bordelese Bottles">Bordelese</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/bottles/bordeaux.html ";?>" title="Bordeaux Bottles">Bordeaux</a></li>
                    </ul>
                    <h3>Bottle Size</h3>
                    <ul>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/bottles/50-ml.html ";?>" title="50ML Bottles">50ML</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/bottles/60-ml.html ";?>" title="60ML Bottles">60ML</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/bottles/100-ml.html ";?>" title="100ML Bottles">100ML</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/bottles/200-ml.html ";?>" title="200ML Bottles">200ML</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/bottles/250-ml.html ";?>" title="250ML Bottles">250ML</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/bottles/375-ml.html ";?>" title="375ML Bottles">375ML</a></li>
	                    <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/bottles/500-ml.html ";?>" title="500ML Bottles">500ML</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/bottles/750-ml.html ";?>" title="750ML Bottles">750ML</a></li>
                    </ul>
                </div>
                <div class="nav-column">
                    <h3>Caps</h3>
                    <ul>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/caps/pet.html ";?>" title="PET Caps">PET</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/caps/serenade.html ";?>" title="Serenade Caps">Serenade</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/caps/stephanie.html ";?>" title="Stephanie Caps">Stephanie</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/caps/marasca.html ";?>" title="Marasca Caps">Marasca</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/caps/dorica.html ";?>" title="Dorica Caps">Dorica</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/caps/bordolese.html ";?>" title="Bordolese Caps">Bordolese</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/caps/bordeaux.html ";?>" title="Bordeaux Caps">Bordeaux</a></li>
                    </ul>
                    <h3>Security Seals</h3>
                    <ul>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/security-seals.html "; ?>" title="Security Seals">Security Seals</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/caps/capsules.html "; ?>" title="Capsules">Capsules</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/sleeves.html "; ?>" title="Sleeves">Sleeves</a></li>
                    </ul>
                    <h3>Cap Size</h3>
                    <ul>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/caps/50ml.html ";?>" title="50ML Caps">50ML</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/caps/60ml.html ";?>" title="60ML Caps">60ML</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/caps/100ml.html ";?>" title="100ML Caps">100ML</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/caps/200ml.html ";?>" title="200ML Caps">200ML</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/caps/250ml.html ";?>" title="250ML Caps">250ML</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/caps/375ml.html ";?>" title="375ML Caps">375ML</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/caps/500ml.html ";?>" title="500ML Caps">500ML</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/caps/750ml.html ";?>" title="750ML Caps">750ML</a></li>
                    </ul>
                </div>
                <div class="nav-column">
                    <h3>Micellaneous</h3>
                    <ul>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/miscellaneous.html ";?>" title="View All">View All</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/dipping-bowls.html "?>" title="Dipping Bowls">Dipping Bowls</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/gift-boxes.html "?>" title="Gift Boxes">Gift Boxes</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/pumps.html "?>" title="Pumps">Pumps</a></li>
	                    <li><a href="<?php echo Mage::getBaseUrl() . "salt-sisters/ ";?>" title="Salt sisters">s.a.l.t. sisters</a></li>
	                    <li><a href="<?php echo Mage::getBaseUrl() . "samples.html "?>" title="Samples">Samples</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/stickers.html "?>" title="Stickers">Stickers</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "olive-oil-store-accessories/oil-and-wine-savers.html "?>" title="Vacuum Sealers">Vacuum Sealers</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li><a href="<?php echo Mage::getBaseUrl() . "why-us ";?>" title="Why Us">Why Us</a></li>
        <li><a href="<?php echo Mage::getBaseUrl() . "faq ";?>" title="FAQ">FAQ</a>
            <div>
                <div class="nav-column" style="width:40%">
                    <h3>FAQ</h3>
                    <ul>
                        <li><a href="<?php echo Mage::getBaseUrl() . "are-balsamic-vinegar-products-vegan ";?>">Are the Balsamic Vinegar products Vegan</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "25-star-balsamic-vinegar ";?>">25 Star Balsamic Vinegar</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "300-minimum ";?>">$300 Minimum</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "basic-tasting-evaluation-guide ";?>">Basic Tasting Evaluation Guide</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "certified-organic-faq ";?>">Certified Organic FAQ</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "espresso-bean-balsamic-vinegar-faq ";?>">Espresso Bean Balsamic Vinegar FAQ</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "fusti-cleaning ";?>">Fusti Cleaning</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "how-to-shrink-sleeves-capsules ";?>">How to Shrink Sleeves/Capsules</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "how-do-i-know-which-olive-oils-are-good ";?>" title="How Do I Know Which Olive Oils are Good">How Do I Know Which Olive Oils are Good</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "how-to-order ";?>" title="How To Order">How To Order</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "how-much-do-the-olive-oil-sprayers-hold ";?>" title="How Much do the Olive Oil Sprayers Hold">How Much do the Olive Oil Sprayers Hold</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "password-request/ ";?>" title="How Do I Request A New Password">How Do I Request A New Password</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "infused-flavored-fused-olive-oils ";?>">Infused, Flavored and Fused Olive Oil</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "polyphenols ";?>" title="Polyphenols">Polyphenols</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "shelf-life ";?>" title="Shelf Life">Shelf Life</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "organic-butter-oil-flavor-faq ";?>" title="What is the ingredient listing for the butter flavored EVOO">What is the ingredient listing for the butter flavored EVOO</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "chili-type ";?>" title="How To Order">What type of Chili is used for your Roasted Chili Infused Olive Oil</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "credit-card-payments ";?>" title="When Do I Pay">When Do I Pay</a></li>
                        <li><a href="<?php echo Mage::getBaseUrl() . "first-order-with-cibaria ";?>">Your First Order With Cibaria</a></li>
                    </ul>
    </ul>
    </li>
    </div>
    </div>
    </li>
    </ul>
</div>