<?php 

$functions_path = $_SERVER['DOCUMENT_ROOT'];
$functions_path .= '/shop/includes/functions.php';
$session = Mage::getSingleton("customer/session", ['name' => 'frontend']);
?>

<div class="top-header-main-mobile"></div>
<div class="top-header-main">
    <div class="top-header-container">
        <br />
        <div>
            <?php
                //loggedin

                if ($session->isLoggedIn()) { ?>
                <a href="<?php echo Mage::getBaseUrl() . "sales/order/history"; ?>" rel="nofollow" class="top-buttons">Check My Order Status</a>
                <a href="<?php echo Mage::getBaseUrl() . "customer/account"; ?>" rel="nofollow" class="top-buttons">My Account</a>
                <?php } else { ?>
                        <a href="<?php echo Mage::getBaseUrl() . "request-account"; ?>" class="top-buttons" rel="nofollow">Request An Account</a>
                <a href="<?php echo Mage::getBaseUrl() . "customer/account/login "; ?>" class="top-buttons" rel="nofollow">Login</a>
                <?php } ?>
                <?php
                    if ($session->isLoggedIn()) { ?>
                    <a href="<?php echo Mage::getBaseUrl() . "customer/account/logout"; ?>" rel="nofollow" class="top-buttons">Logout</a>
                    <?php }else { ?>
                    <?php } ?>
                    <div id="search" style="float:right;margin-top:0px;">
                        <div class="button-search"></div>
                        <form id="search_mini_form" action="<?php echo Mage::getBaseUrl() . "catalogsearch/result/";?>" rel="nofollow" method="get">
                            <input id="search" type="text" class="input-text" name="q" value="" placeholder="Search">
                        </form>
                    </div>
        </div>
    </div>
</div>
<div id="header-area">
    <div id="header-wrapper">
        <div id="header">
            <img src="<?php echo Mage::getBaseUrl(); ?>skin/frontend/default/cibaria-supply/images/header-left.png" alt="Gourmet Olive Oils & Vinegars" style="float:left;" />
            <a href="<?php echo get_homepage(); ?>"><img src="<?php echo Mage::getBaseUrl(); ?>skin/frontend/default/cibaria-supply/images/logo.png" alt="Cibaria Store Supply" id="logo"/></a>
            <?php
            //if logged in for shopping cart
            if ($session->isLoggedIn()) { ?>
                <?php
                //TODO:
                //echo $this->getChildHtml('top_cart'); ?>
                <img src="<?php echo Mage::getBaseUrl(); ?>skin/frontend/default/cibaria-supply/images/header-right.png" alt="Sold Fresh to Olive Oil Stores Across America" style="float:right;" />
            <?php }else{ ?>
                <img src="<?php echo Mage::getBaseUrl(); ?>skin/frontend/default/cibaria-supply/images/header-right.png" alt="Sold Fresh to Olive Oil Stores Across America" style="float:right;" />
                <?php } ?>
        </div>
    </div>
</div>
<!-- end header //-->
<div id="header-area-mobile">
    <a href="<?php echo Mage::getBaseUrl(); ?>"><img src="<?php echo Mage::getBaseUrl(); ?>skin/frontend/default/cibaria-supply/images/logo.png" alt="Cibaria Store Supply" id="logo" class="header-area-mobile-logo"/></a>
</div>
<?php
//if logged in desktop nav
if ($session->isLoggedIn()) { ?>
    <?php include(url_path('/shop/includes/logged-in-nav.php')); ?>
    <?php }else{ ?>
    <?php include(url_path('/shop/includes/nav.php')); ?>
    <?php } ?>
    <?php
//if logged in mobile nav
if ($session->isLoggedIn()) { ?>
        <?php include(url_path('/shop/includes/logged-in-mobile-nav.php')); ?>
        <?php }else{ ?>
        <?php include(url_path('/shop/includes/mobile-nav.php')); ?>
        <?php } ?>