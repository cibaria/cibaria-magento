<?php

/* Paths */
function url_path($url) {
	return $_SERVER['DOCUMENT_ROOT'] . $url;
}

function get_homepage() {
    return 'http://www.cibariastoresupply.com';
}

function getRecipeImageUrl($image) {
    $url = "https://www.cibariainternational.com/images/recipes/";
    $get_image = $url . $image;
    //
    return $get_image;
}

//Get the Left Navigation for Admin Pages
function adminPaths() {
    $currentUrl = Mage::helper('core/url')->getCurrentUrl();
    $url = Mage::getSingleton('core/url')->parseUrl($currentUrl);
    $path = $url->getPath();
    $adminpath = '';

    $adminPaths = [
        '/shop/customer/account/index/',
        '/shop/customer/account/index',
        '/shop/wishlist/',
        '/shop/customer/account/edit/',
        '/shop/customer/address/index/',
        '/shop/sales/order/history/'
    ];

    if(in_array($path, $adminPaths)) {
        $adminpath = true;
    } else {
        $adminpath = false;
    }
    return $adminpath;
}

/*Product Loops*/
function getCurrentCategories() {
    //Categories
    $category = Mage::getSingleton('catalog/layer')->getCurrentCategory();
    $categories = $category->getCollection()
        ->addAttributeToSelect(array('name', 'thumbnail'))
        ->addAttributeToFilter('is_active', 1)
        ->addAttributeToSort('name', 'ASC')
        ->addIdFilter($category->getChildren());
    return $categories;
}

function getProductCategories($categoryID)
{
    $featured = Mage::getModel('catalog/category')->load($categoryID);
    $featured = $featured->getProductCollection();
    return $featured->getAllIds();
}
/*--------------*/

/* Recipes */
function showRecipeImage($image) {
    $url = "https://www.cibariainternational.com/images/recipes/";
    $get_image = $url . $image;
    $imageData = base64_encode(file_get_contents($get_image));
    $image_src = 'data: '.mime_content_type($get_image).';base64,'.$imageData;
//
    return $image_src;
}

/*Nutrition Label*/
include_once('api-calls.php');

//oils
function get_all_oils_title($oils) {
    foreach ($oils as $result) {
        return $result['title'];
    }
}

//vinegars
function get_all_vinegars_title($vinegars) {
    foreach ($vinegars as $result) {
        return $result['title'];
    }
}

//25 star white balsamic Vinegar
function get_all_25_star_white($white_25_star) {
    foreach ($white_25_star as $result) {
        return $result['title'];
    }
}

//25 star dark balsamic vinegar
function get_all_25_star_dark($dark_25_star) {
    foreach ($dark_25_star as $result) {
        return $result['title'];
    }
}

//pantry items
function get_all_pantry_items_title($pantry_items) {
    foreach ($pantry_items as $result) {
        return $result['title'];
    }
}

/* Browsers */

//Internet Explorer
function isIE() {
    //detect IE
    $u = $_SERVER['HTTP_USER_AGENT'];
    $isIE7  = (bool)preg_match('/msie 7./i', $u );
    $isIE8  = (bool)preg_match('/msie 8./i', $u );
    $isIE9  = (bool)preg_match('/msie 9./i', $u );
    $isIE10 = (bool)preg_match('/msie 10./i', $u );
    $isIE11 = (bool)preg_match('/Trident\/7.0; rv:11.0/', $u );

    if($isIE7 || $isIE8 || $isIE9 || $isIE10 || $isIE11)
    {
        $isIE = true;
    } else {
        $isIE = false;
    }

    return $isIE;
}

//Mobile Phone
function isMobilePhone() {
    require('Mobile_Detect.php');
    $detect = new Mobile_Detect();

    $is_mobile = $detect->isMobile();

    return $is_mobile;
}
//Tablet
function isTablet() {
    require('Mobile_Detect.php');
    $detect = new Mobile_Detect();

    $is_tablet = $detect->isTablet();

    return $is_tablet;
}

/* -------Mage Setup------ */
if(defined('DS')) { } else {
require(url_path('/shop/app/Mage.php'));
//Set the current Store
Mage::app()->setCurrentStore(1);
//	Mage::app();
}

//login?
Mage::getSingleton("core/session", array("name" => "frontend"));
$session = Mage::getSingleton("customer/session");
/*---------------------------- */