<?php
//required files
require('../includes/admin/db_connect.php');
require('../includes/admin/magenot.php');
if ($session->isLoggedIn()) {
    echo "is logged in";
} else {
    echo "<script type='text/javascript'>window.location.assign('index.php')</script>";
}
?>
<br/>
<br/>
<?php

$id = (int)$_POST['id'];
$title = htmlspecialchars(addslashes($_POST["title"]));
$description = htmlspecialchars(addslashes($_POST["description"]));

//update db_connect.php
try {
    update($db, $id, $title, $description);
    echo "<script type='text/javascript'>window.location.assign('index.php')</script>";

} catch (Exception $e) {
    echo $e;
}

?>
