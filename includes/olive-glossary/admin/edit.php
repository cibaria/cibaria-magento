<?php
//required files
require('../includes/admin/db_connect.php');
require('../includes/admin/magenot.php');
if ($session->isLoggedIn()) {
    include('../design/admin/header.php');
    include('../design/admin/body.php');
} else {
    echo "<script type='text/javascript'>window.location.assign('index.php')</script>";
}
?>
    <br/>
    <br/>
<?php
//local development
mysql_connect('localhost', 'root', 'advanced') or die(mysql_error());
//production
//mysql_connect('localhost', 'root', '') or die(mysql_error());

mysql_select_db("glossary") or die(mysql_error());

//get the id from post
$id = $_REQUEST['id'];
//get everything
$query = mysql_query("SELECT * FROM olive_glossary WHERE id = '$id'");
if (mysql_num_rows($query) >= 1) {
    try {
        while ($row = mysql_fetch_array($query)) {
            $title = $row['title'];
            $description = $row['description'];
        }
    } catch (Exception $e) {

        echo "<pre>" . $e . "</pre>";
    }
    ?>
    <div class="content-header" style="visibility: visible;">
        <h3 class="icon-head head-products">Edit <?php echo $title; ?> Olive Glossary Term</h3>
    </div>

    <div id="product_info_tabs_group_4_content" style="">
        <div class="entry-edit">
            <div class="entry-edit-head">
                <h4 class="icon-head head-edit-form fieldset-legend">General</h4>

            </div>
            <div class="fieldset fieldset-wide" id="group_fields4">
                <div class="hor-scroll">
                    <form action="update.php" method="post">
                        <input type="hidden" name="id" value="<?php echo $id; ?>"/>

                        <table cellspacing="0" class="form-list">
                            <tbody>

                            <tr>
                                <td class="label"><label for="name">Title <span class="required">*</span></label></td>
                                <td class="value"><input type="text" name="title" value="<?php echo $title; ?>" class="input-text"/></td>
                            </tr>

                            <tr>
                                <td class="label"><label for="description">Description <span class="required">*</span></label></td>
                                <td class="value"><textarea id="description" name="description" class=" required-entry required-entry textarea" rows="2" cols="15"><?php echo $description; ?></textarea>
                            </tr>
                            <tr>
                                <td><input type="submit" value="Submit"/></td>

                            </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>


<?php } else {
    echo "Sorry there was an issue with your request. Please try again later.";
} ?>