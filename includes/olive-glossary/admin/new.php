<?php
//required files
require('../includes/admin/db_connect.php');
require('../includes/admin/magenot.php');
if ($session->isLoggedIn()) {
    include('../design/admin/header.php');
    include('../design/admin/body.php');
} else {
    echo "<script type='text/javascript'>window.location.assign('index.php')</script>";
}
?>
    <br/>
    <br/>
<?php
if (!empty($_POST['title']) && ($_POST['title'])) {
    try {
        $title = htmlspecialchars(addslashes($_POST['title']));
        $description = htmlspecialchars(addslashes($_POST['description']));
//insert into db
        insert($db, $title, $description);
//javascript redirect kinda ugly but it works
        echo "<script type='text/javascript'>window.location.assign('index.php')</script>";
    } catch (Exception $e) {
        echo $e;
    }
} else {
    echo "";
}
?>

    <div class="content-header" style="visibility: visible;">
        <h3 class="icon-head head-products">New Glossary Term</h3>
    </div>
    <div id="product_info_tabs_group_4_content" style="">
        <div class="entry-edit">
            <div class="entry-edit-head">
                <h4 class="icon-head head-edit-form fieldset-legend">General</h4>

            </div>
            <div class="fieldset fieldset-wide" id="group_fields4">
                <div class="hor-scroll">
                    <form action="" method="post">
                        <table cellspacing="0" class="form-list">
                            <tbody>

                            <tr>
                                <td class="label"><label for="name">Title <span class="required">*</span></label></td>
                                <td class="value"><input type="text" name="title" class="input-text"/></td>
                            </tr>

                            <tr>
                                <td class="label"><label for="description">Description <span class="required">*</span></label></td>
                                <td class="value"><textarea id="description" name="description" class=" required-entry required-entry textarea" rows="2" cols="15"></textarea>
                            </tr>
                            <tr>
                                <td><input type="submit" value="Submit"/></td>

                            </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>



<?php include('../design/admin/footer.php'); ?>