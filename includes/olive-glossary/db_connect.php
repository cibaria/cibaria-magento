<?php
/*
 * The local code is within the main cibaria project (all sites main Magento project)
 * The hosted file is located within the cibariastore site (the cibariastoresupply.com design)
 *
 * Any competent Magento dev. looking at this file must be wondering why. The reason is that
 * the goal is to move cibaria's sites off Magento. I wanted to go with a framework at first but
 * the server is on php 5.2 and there's isn't a modern day PHP framework that I can use to make this work
 * so I decided to use straight php
 */
require('db_connect_info.php');
global $db;
/* ----- Connection Strings for Local and remote ------ */
//remote
//$db = mysqli_connect('localhost', 'root', 'LbUHDX6VUajoDxt', 'glossary');
//local
$db = mysqli_connect(SERVER, USERNAME, PASSWORD, DATABASE);
/*-----------------------------------------------------*/

if (!$db) {
    die('Could Not Connect To The Database');
}

/*
 * Queries
 *
 * All index.php
 * Insert new.php
 * Update update.php
 * Delete
 */

//all
function all($db)
{
    $query_all = mysqli_query($db, "SELECT * FROM olive_glossary ORDER by title ASC");
    if ($query_all == true) {
        while ($row = mysqli_fetch_row($query_all)) {  ?>

            <?php if ($row[2] == 'Aglandau') { ?>
                <div style="border-bottom: solid 1px #B063C3; margin-bottom: 15px; ">
                    <h2 style="color:#B063C3; margin-bottom:-5px;"><a id="A" style="color:#B063C3;">A</a></h2>
                </div>
            <?php } elseif ($row[2] == 'Barnea') { ?>
                <div style="border-bottom: solid 1px #FF8500; margin-bottom: 15px; margin-top:40px; ">
                    <h2 style="color:#FF8500; margin-bottom:-5px;"><a id="B" style="color:#FF8500;">B</a></h2>
                </div>
            <?php } elseif ($row[2] == 'Cailletier') { ?>
                <div style="border-bottom: solid 1px #B063C3; margin-bottom: 15px; margin-top:40px; ">
                    <h2 style="color:#B063C3; margin-bottom:-5px;"><a id="C" style="color:#B063C3;">C</a></h2>
                </div>
            <?php } elseif ($row[2] == 'Darbechtar') { ?>
                <div style="border-bottom: solid 1px #FF8500; margin-bottom: 15px; margin-top:40px; ">
                    <h2 style="color:#FF8500; margin-bottom:-5px;"><a id="D" style="color:#FF8500;">D</a></h2>
                </div>
            <?php } elseif ($row[2] == 'Farga') { ?>
                <div style="border-bottom: solid 1px #B063C3; margin-bottom: 15px; margin-top:40px; ">
                    <h2 style="color:#B063C3; margin-bottom:-5px;"><a id="F" style="color:#B063C3;">F</a></h2>
                </div>
            <?php } elseif ($row[2] == 'Gemlik') { ?>
                <div style="border-bottom: solid 1px #FF8500; margin-bottom: 15px; margin-top:40px; ">
                    <h2 style="color:#FF8500; margin-bottom:-5px;"><a id="G" style="color:#FF8500;">G</a></h2>
                </div>
            <?php } elseif ($row[2] == 'Hojiblanca') { ?>
                <div style="border-bottom: solid 1px #B063C3; margin-bottom: 15px; margin-top:40px; ">
                    <h2 style="color:#B063C3; margin-bottom:-5px;"><a id="H" style="color:#B063C3;">H</a></h2>
                </div>
            <?php } elseif ($row[2] == 'Izmir Sofralik') { ?>
                <div style="border-bottom: solid 1px #FF8500; margin-bottom: 15px; margin-top:40px; ">
                    <h2 style="color:#FF8500; margin-bottom:-5px;"><a id="I" style="color:#FF8500;">I</a></h2>
                </div>
            <?php } elseif ($row[2] == 'Kalamata') { ?>
                <div style="border-bottom: solid 1px #B063C3; margin-bottom: 15px; margin-top:40px; ">
                    <h2 style="color:#B063C3; margin-bottom:-5px;"><a id="K" style="color:#B063C3;">K</a></h2>
                </div>
            <?php } elseif ($row[2] == 'Leccino') { ?>
                <div style="border-bottom: solid 1px #FF8500; margin-bottom: 15px; margin-top:40px; ">
                    <h2 style="color:#FF8500; margin-bottom:-5px;"><a id="L" style="color:#FF8500;">L</a></h2>
                </div>
            <?php } elseif ($row[2] == 'Maalot') { ?>
                <div style="border-bottom: solid 1px #B063C3; margin-bottom: 15px; margin-top:40px; ">
                    <h2 style="color:#B063C3; margin-bottom:-5px;"><a id="M" style="color:#B063C3;">M</a></h2>
                </div>
            <?php } elseif ($row[2] == 'Nabali') { ?>
                <div style="border-bottom: solid 1px #FF8500; margin-bottom: 15px; margin-top:40px; ">
                    <h2 style="color:#FF8500; margin-bottom:-5px;"><a id="N" style="color:#FF8500;">N</a></h2>
                </div>
            <?php } elseif ($row[2] == 'Ogliarola') { ?>
                <div style="border-bottom: solid 1px #B063C3; margin-bottom: 15px; margin-top:40px; ">
                    <h2 style="color:#B063C3; margin-bottom:-5px;"><a id="O" style="color:#B063C3;">O</a></h2>
                </div>
            <?php } elseif ($row[2] == 'Patrinia') { ?>
                <div style="border-bottom: solid 1px #FF8500; margin-bottom: 15px; margin-top:40px; ">
                    <h2 style="color:#FF8500; margin-bottom:-5px;"><a id="P" style="color:#FF8500;">P</a></h2>
                </div>
            <?php } elseif ($row[2] == 'Rotondella') { ?>
                <div style="border-bottom: solid 1px #B063C3; margin-bottom: 15px; margin-top:40px; ">
                    <h2 style="color:#B063C3; margin-bottom:-5px;"><a id="R" style="color:#B063C3;">R</a></h2>
                </div>
            <?php } elseif ($row[2] == 'Salonenque') { ?>
                <div style="border-bottom: solid 1px #FF8500; margin-bottom: 15px; margin-top:40px; ">
                    <h2 style="color:#FF8500; margin-bottom:-5px;"><a id="S" style="color:#FF8500;">S</a></h2>
                </div>
            <?php } elseif ($row[2] == 'Tanche') { ?>
                <div style="border-bottom: solid 1px #B063C3; margin-bottom: 15px; margin-top:40px; ">
                    <h2 style="color:#B063C3; margin-bottom:-5px;"><a id="T" style="color:#B063C3;">T</a></h2>
                </div>
            <?php } elseif ($row[2] == 'Verdial') { ?>
                <div style="border-bottom: solid 1px #FF8500; margin-bottom: 15px; margin-top:40px; ">
                    <h2 style="color:#FF8500; margin-bottom:-5px;"><a id="V" style="color:#FF8500;">V</a></h2>
                </div>

            <?php } ?>

            <div style='margin-bottom:15px; border-bottom: 1px dotted grey;'>
                <h3 style='margin-bottom: 1px;'>
                    <?php echo $row[2]; ?>
                </h3>
                <?php echo $row[1]; ?>
                <br/></div>
        <?php
        }
    } else {
        ?>
        <h1>No Results</h1>
    <?php }
} ?>

<?php
//insert

function insert($db, $title, $description)
{
    $insert_into = mysqli_query($db, "INSERT INTO olive_glossary (id, title, description) VALUES('', '$title','$description')");
}

//update
function update($db, $id, $title, $description)
{
    $update = mysqli_query($db, "UPDATE olive_glossary SET title = '$title', description = '$description' WHERE id='$id'");
    mysqli_query($db, $update)
    or die(mysql_error());
}

//delete
function delete($db, $id)
{
    mysqli_query($db, "DELETE FROM definitions WHERE id = '$id'")
    or die(mysql_error());
    echo "<script type='text/javascript'>window.location.assign('index.php')</script>";
}

/* =========================== This Function works to grab everything with php 5.3 not 5.2 ========================*/
// function all($query){
//     foreach($query as $result){
//         echo "Title:" . $result['title'] . "<br />";
//         echo "Description: " . $result['description'] . "<br /><hr />";
//     }
// }
/* ================================================================================================================*/


