<div id="footer">

    <!-- Begin contacts //-->

    <div class="column-contacts">

        <ul>

            <h3>Cibaria International</h3>

            <li class="phone">1203 Hall Ave. Riverside, CA 92509</li>

            <li class="phone">Phone: (951) 823 - 8490</li>

            <li class="mobile">Fax: (951) 823 - 8495</li>

            <li class="email"><a href="mailto:customerservice@cibaria-intl.com">Email Us</a></li>

        </ul>

    </div>

    <!-- End contacts //-->

    <!-- Begin footer columns //-->

    <div class="column4">

        <h3>Cibaria Websites</h3>

        <ul>

            <li><a href="http://www.cibariasoapsupply.com/shop/">Cibaria Soap Supply</a></li>

            <li><a href="http://blog.cibariasoapsupply.com/">Soap Supply Blog</a></li>

            <li><a href="http://www.cibaria-intl.com">Cibaria International</a></li>

        </ul>

    </div>

    <div class="column3">

        <h3>My Account</h3>

        <ul>

            <li><a href="/shop/customer/account">My Account</a></li>

            <li><a href="/shop/sales/order/history/">Order History</a></li>

            <li><a href="/shop/wishlist">Wish List</a></li>

        </ul>

    </div>

    <div class="column2">

        <h3>Customer Service</h3>

        <ul>

            <li><a href="/shop/contacts">Contact Us</a></li>

            <li><a href="/shop/freight-policy">Freight Policy</a></li>

            <li><a href="/shop/payments">Website Payment Procedure</a></li>

            <li><a href="/shop/ordering-policy">Ordering Policy</a></li>

            <li><a href="/shop/damage-policy">Damage Policy</a></li>

            <li><a href="/shop/policy-changes">Policy Changes</a></li>

            <li><a href="/shop/privacy-policy">Privacy Policy</a></li>

            <li><a href="/shop/site-terms">Terms &amp; Conditions</a></li>

        </ul>

    </div>

    <div class="column1">

        <h3>Information</h3>

        <ul>

            <li><a href="/shop/about-us">About Us</a></li>

            <li><a href="/shop/index.php/faq">FAQ</a></li>

            <li><a href="/shop/company-holidays">Company Holidays</a></li>

            <li><a href="/shop/index.php/articles">Articles</a></li>

        </ul>

    </div>



</div>

<div id="powered" style="padding-left:50px;">

    Copyright &copy; <?php echo date("Y"); ?> Cibaria Store Supply - All Rights Reserved

    <div style="clear:both"></div>

</div>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-30465466-2', 'cibariastoresupply.com');
  ga('send', 'pageview');

</script>

</div>

</div>