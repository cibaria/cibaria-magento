<br />
<div class="box">
<div class="box-heading">
	<span class="specials">Company News</span>
<br />
	
</div>

<div style="margin-top:25px;"></div>
<?php
$rss = new DOMDocument();
$test = $rss->load('http://www.cibaria-intl.com/rss.xml');


$feed = array();
foreach ($rss->getElementsByTagName('item') as $node) {
    $item = array (
        'title' => $node->getElementsByTagName('title')->item(0)->nodeValue,
        'desc' => $node->getElementsByTagName('description')->item(0)->nodeValue,
        'link' => $node->getElementsByTagName('link')->item(0)->nodeValue,
        'date' => $node->getElementsByTagName('pubDate')->item(0)->nodeValue,
    );
    array_push($feed, $item);
}

$limit = 5;
for($x=0;$x<$limit;$x++) {
    if(isset($feed[$x]['title']))
    {
        $title = str_replace(' & ', ' &amp; ', $feed[$x]['title']);
    }
    if(isset($feed[$x]['link']))
    {
        $link = $feed[$x]['link'];
    }
    if(isset($feed[$x]['desc']))
    {
        $description = $feed[$x]['desc'];
    }
    if(isset($feed[$x]['date']))
    {
        $date = date('l F d, Y', strtotime($feed[$x]['date']));
    }
    echo '<p><strong><a href="http://www.cibaria-intl.com'.$link.'" title="'.$title.'">'.$title.'</a></strong><br />';
    echo '<small><em>Posted on '.$date.'</em></small><br /><hr /></p>';
    //echo '<p>'.substr($description, 200).'</p>';
    // echo '<hr />';
}


?>
</div>