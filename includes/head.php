<meta name="description" content="Cibaria Store Supply"/>
<link href="#" rel="icon"/>

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>

<link rel="stylesheet" type="text/css" href="<?php echo Mage::getBaseUrl()?>skin/frontend/default/cibaria-supply/css/stylesheet.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo Mage::getBaseUrl()?>skin/frontend/default/cibaria-supply/css/nav.css"/>
<link rel="stylesheet" media="all" href="<?php echo Mage::getBaseUrl()?>skin/frontend/default/cibaria-supply/css/mobile.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo Mage::getBaseUrl()?>skin/frontend/default/cibaria-supply/css/cloud-zoom.css"/>
<link type="text/css" rel="stylesheet" href="<?php echo Mage::getBaseUrl()?>skin/frontend/default/cibaria-supply/css/jquery.qtip.css"/>

<script type="text/javascript" src="<?php echo Mage::getBaseUrl()?>skin/frontend/default/cibaria-supply/js/scroll-to-top.js"></script>
<script type="text/javascript" src="<?php echo Mage::getBaseUrl()?>skin/frontend/default/cibaria-supply/js/menu-mobile.js"></script>
<script type="text/javascript" src="<?php echo Mage::getBaseUrl()?>skin/frontend/default/cibaria-supply/js/footer-menu-mobile.js"></script>
<script type="text/javascript" src="<?php echo Mage::getBaseUrl()?>skin/frontend/default/cibaria-supply/js/thumbnail-hover.js"></script>
<script type="text/JavaScript" src="<?php echo Mage::getBaseUrl()?>skin/frontend/default/cibaria-supply/js/cloud-zoom.1.0.2.js"></script>
<script type="text/JavaScript" src="<?php echo Mage::getBaseUrl()?>skin/frontend/default/cibaria-supply/js/curr-lang-dropdown.js"></script>

<meta name="viewport" content="width=device-width, initial-scale=1"/>
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<link rel="stylesheet" type="text/css" href="<?php echo Mage::getBaseUrl()?>skin/frontend/default/cibaria-supply/css/slideshow.css" media="screen"/>
<link rel="stylesheet" type="text/css" href="<?php echo Mage::getBaseUrl()?>skin/frontend/default/cibaria-supply/css/carousel.css" media="screen"/>

<script type="text/javascript" src="<?php echo Mage::getBaseUrl()?>skin/frontend/default/cibaria-supply/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<?php echo Mage::getBaseUrl()?>skin/frontend/default/cibaria-supply/js/jquery-ui-1.8.16.custom.min.js"></script>

<!--<link rel="stylesheet" type="text/css" href="--><?php //echo Mage::getBaseUrl()?><!--skin/frontend/default/cibaria-supply/js/jquery-ui-1.8.16.custom.css"/>-->
<script type="text/javascript" src="<?php echo Mage::getBaseUrl()?>skin/frontend/default/cibaria-supply/js/jquery.cookie.js"></script>
<script type="text/javascript" src="<?php echo Mage::getBaseUrl()?>skin/frontend/default/cibaria-supply/js/jquery.colorbox.js"></script>

<!-- <link rel="stylesheet" type="text/css" href="<?php echo Mage::getBaseUrl()?>skin/frontend/default/cibaria-supply/css/coslorbox.css" media="screen"/> -->

<!-- <script type="text/javascript" src="<?php echo Mage::getBaseUrl()?>skin/frontend/default/cibaria-supply/js/jquery/tabs.js"></script> -->
<script type="text/javascript" src="<?php echo Mage::getBaseUrl()?>skin/frontend/default/cibaria-supply/js/common.js"></script>
<script type="text/javascript" src="<?php echo Mage::getBaseUrl()?>skin/frontend/default/cibaria-supply/js/jquery.nivo.slider.pack.js"></script>
<script type="text/javascript" src="<?php echo Mage::getBaseUrl()?>skin/frontend/default/cibaria-supply/js/jquery.cycle.js"></script>
<script type="text/javascript" src="<?php echo Mage::getBaseUrl()?>skin/frontend/default/cibaria-supply/js/jquery.jcarousel.min.js"></script>
<!--[if IE 7]>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/elegium/stylesheet/ie7.css"/>
<![endif]-->
<!--[if lt IE 7]>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/elegium/stylesheet/ie6.css"/>
<script type="text/javascript" src="catalog/view/javascript/DD_belatedPNG_0.0.8a-min.js"></script>
<script type="text/javascript">
    DD_belatedPNG.fix('#logo img');
</script>
<![endif]-->

<link href='//fonts.googleapis.com/css?family=Ubuntu:400,700' rel='stylesheet' type='text/css'>