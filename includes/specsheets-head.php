<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Specsheets</title>
    <link media="all" type="text/css" rel="stylesheet" href="<?php echo Mage::getBaseUrl() . 'css/bootstrap.css' ?>"/>
    <link media="print" type="text/css" rel="stylesheet" href="<?php echo Mage::getBaseUrl() . 'css/print.css' ?>"/>
    <script src="<?php echo Mage::getBaseUrl(); ?>js/jquery-1.9.1.js"></script>
    <script src="<?php echo Mage::getBaseUrl(); ?>js/jquery-ui.js"></script>
    <link rel="stylesheet" href="<?php echo Mage::getBaseUrl(); ?>css/jquery-ui.css"/>
     <style>
        body {
            margin: 40px;
            -webkit-print-color-adjust: exact;
            font-size: 13px;
        }

        table {
            font-size: 13px;
        }
    </style>
</head>