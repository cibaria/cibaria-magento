<?php

//$autoload = $_SERVER['DOCUMENT_ROOT'] . '/shop/includes/vendor/autoload.php';
include(url_path('/shop/includes/vendor/autoload.php'));

//require($autoload);

//use Guzzle for api get
$client = new GuzzleHttp\Client();
//nutrition facts
$oils_body = $client->get('https://www.cibariainternational.com/api/nutritionfacts/oils')->getBody()->getContents();
$vinegar_body = $client->get('https://www.cibariainternational.com/api/nutritionfacts/vinegars')->getBody()->getContents();
$white_25_star_body = $client->get('https://www.cibariainternational.com/api/nutritionfacts/white_25_star')->getBody()->getContents();
$dark_25_star_body = $client->get('https://www.cibariainternational.com/api/nutritionfacts/dark_25_star')->getBody()->getContents();
$pantry_items_body = $client->get('https://www.cibariainternational.com/api/nutritionfacts/pantry_items')->getBody()->getContents();
$specialty_items_body = $client->get('https://www.cibariainternational.com/api/nutritionfacts/pantry_items')->getBody()->getContents();

//recipes
$recipes_body = $client->get('https://www.cibariainternational.com/api/recipes')->getBody()->getContents();

//specsheets
$specsheets_body = $client->get('https://www.cibariainternational.com/api/specsheets')->getBody()->getContents();

//nutrition facts
$oils = json_decode($oils_body, true);
$vinegars = json_decode($vinegar_body, true);
$white_25_star = json_decode($white_25_star_body, true);
$dark_25_star = json_decode($dark_25_star_body, true);
$pantry_items = json_decode($pantry_items_body, true);
$specialty_items = json_decode($specialty_items_body, true);

//recipes
$recipes = json_decode($recipes_body, true);

//specsheets
$specsheets = json_decode($specsheets_body, true);
