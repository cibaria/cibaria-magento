    <?php foreach ($vinegars as $result) {?>
    <?php if ($result['active'] == '1') {?>
    <tr>
    <form action="<?php echo Mage::getBaseUrl(); ?>nutrition-labels/view.php" method="post">
        <input type="hidden" value="<?php echo $result['title']; ?>" name="title" />
        <input type="hidden" value="<?php echo $result['serving_size']; ?>" name="serving_size" />
        <input type="hidden" value="<?php echo $result['servings_per_container']; ?>" name="servings_per_container" />
        <input type="hidden" value="<?php echo $result['calories']; ?>" name="calories" />
        <input type="hidden" value="<?php echo $result['calories_from_fat']; ?>" name="calories_from_fat" />
        <input type="hidden" value="<?php echo $result['total_fat_grams']; ?>" name="total_fat_grams" />
        <input type="hidden" value="<?php echo $result['total_fat_percent']; ?>" name="total_fat_percent" />
        <input type="hidden" value="<?php echo $result['saturated_fat_grams']; ?>" name="saturated_fat_grams" />
        <input type="hidden" value="<?php echo $result['saturated_fat_percentage'] ?>" name="saturated_fat_percentage" />
        <input type="hidden" value="<?php echo $result['trans_fat']; ?>" name="trans_fat" />
        <input type="hidden" value="<?php echo $result['cholesterol_grams']; ?>" name="cholesterol_grams" />
        <input type="hidden" value="<?php echo $result['cholesterol_percentage']; ?>" name="cholesterol_percentage" />
        <input type="hidden" value="<?php echo $result['sodium_grams']; ?>" name="sodium_grams" />
        <input type="hidden" value="<?php echo $result['sodium_percentage']; ?>" name="sodium_percentage" />
        <input type="hidden" value="<?php echo $result['total_carbohydrates_grams']; ?>" name="total_carbohydrates_grams" />
        <input type="hidden" value="<?php echo $result['total_carbohydrates_percentage']; ?>" name="total_carbohydrates_percentage" />
        <input type="hidden" value="<?php echo $result['dietary_fiber_grams']; ?>" name="dietary_fiber_grams" />
        <input type="hidden" value="<?php echo $result['dietary_fiber_percentage']; ?>" name="dietary_fiber_percentage" />
        <input type="hidden" value="<?php echo $result['sugars_grams']; ?>" name="sugars_grams" />
        <input type="hidden" value="<?php echo $result['protein_grams']; ?>" name="protein_grams" />
        <input type="hidden" value="<?php echo $result['blurb']; ?>" name="blurb" />
        <input type="hidden" value="<?php echo $result['ingredients']; ?>" name="ingredients" />
        <input type="hidden" value="<?php echo $result['added_sugars']; ?>" name="added_sugars" />
        <input type="hidden" value="<?php echo $result['vitamin_d_grams']; ?>" name="vitamin_d_grams" />
        <input type="hidden" value="<?php echo $result['vitamin_d_percentage']; ?>" name="vitamin_d_percentage" />
        <input type="hidden" value="<?php echo $result['calcium_grams']; ?>" name="calcium_grams" />
        <input type="hidden" value="<?php echo $result['calcium_percentage']; ?>" name="calcium_percentage" />
        <input type="hidden" value="<?php echo $result['iron_grams']; ?>" name="iron_grams" />
        <input type="hidden" value="<?php echo $result['iron_percentage']; ?>" name="iron_percentage" />
        <input type="hidden" value="<?php echo $result['potassium_grams']; ?>" name="potassium_grams" />
        <input type="hidden" value="<?php echo $result['potassium_percentage']; ?>" name="potassium_percentage" />

        <td style="text-align: center;"><img src="<?php echo Mage::getBaseUrl(); ?>images/nutrition-labels/small/nutrition-label.jpg" alt=" " title=""></a></td>
        <td><?php echo $result['title']; ?></td>
        <td><input type="image" src="<?php echo Mage::getBaseUrl(); ?>images/nutrition-labels/view.gif" alt="View" /> </td>
      </tr>
    </form>
    <?php } } ?>