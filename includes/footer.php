<div id="footer">
    <div class="column-contacts">
        <h3>Cibaria International</h3>
        <ul>
            <li class="phone">(+951) 823 8490</li>
            <li class="email"><a href="<?php echo Mage::getBaseUrl() . "contact/"; ?>" rel="nofollow">customerservice@cibaria-intl.com</a></li>
        </ul>
    </div>
    <div class="column4">
        <h3>Cibaria Stores</h3>
        <ul>
            <li><a href="https://www.cibariasoapsupply.com/shop/" title="Wholesale Soap Making Oils">Wholesale Soap Making Oils</a></li>
            <li><a href="https://www.cibaria-intl.com" title="Wholesale Olive Oils">Wholesale Olive Oils</a></li>
        </ul>
    </div>
    <div class="column3">
        <h3>My Account</h3>
        <ul>
            <li><a href="<?php echo Mage::getBaseUrl() . "customer/account/"; ?>" rel="nofollow" title="My Account">My Account</a></li>
            <li><a href="<?php echo Mage::getBaseUrl() . "sales/order/history/"; ?>" rel="nofollow" title="Order History">Order History</a></li>
        </ul>
    </div>
    <div class="column2">
        <h3>Customer Service</h3>
        <ul>
            <li><a href="<?php echo Mage::getBaseUrl() . "contact/"; ?>" rel="nofollow" title="Contact Us">Contact Us</a></li>
            <li><a href="<?php echo Mage::getBaseUrl() . "credit-card-payments/"; ?>" title="Credit Card Payments">Credit Card Payments</a></li>
            <li><a href="<?php echo Mage::getBaseUrl() . "freight-policy"; ?>" rel="nofollow" title="Freight Policy">Freight Policy</a></li>
            <li><a href="<?php echo Mage::getBaseUrl() . "payments "; ?>" rel="nofollow" title="Website Payment Procedure">Website Payment Procedure</a></li>
            <li><a href="<?php echo Mage::getBaseUrl() . "300-minimum"; ?>" rel="nofollow" title="Minimum Order">Minimum Order</a></li>
            <li><a href="<?php echo Mage::getBaseUrl() . "ordering-policy"; ?>" rel="nofollow" title="Ordering Policy">Ordering Policy</a></li>
            <li><a href="<?php echo Mage::getBaseUrl() . "damage-policy"; ?>" rel="nofollow" title="Damage Policy">Damage Policy</a></li>
            <li><a href="<?php echo Mage::getBaseUrl() . "policy-changes"; ?>" rel="nofollow" title="Policy Changes">Policy Changes</a></li>
            <li><a href="<?php echo Mage::getBaseUrl() . "privacy-policy"; ?>" rel="nofollow" title="Privacy Policy">Privacy Policy</a></li>
            <li><a href="<?php echo Mage::getBaseUrl() . "site-terms"; ?>" rel="nofollow" title="Terms and Conditions">Terms & Conditions</a></li>
        </ul>
    </div>
    <div class="column1">
        <h3>Information</h3>
        <ul>
            <li><a href="<?php echo Mage::getBaseUrl() . "about-us"; ?>" title="About Us">About Us</a></li>
            <li><a href="<?php echo Mage::getBaseUrl() . "faq"; ?>" title="FAQ">FAQ</a></li>
            <li><a href="<?php echo Mage::getBaseUrl() . "company-holidays"; ?>" title="Company Holidays">Company Holidays</a></li>
            <li><a href="<?php echo Mage::getBaseUrl() . "articles"; ?>" title="Articles">Articles</a></li>
        </ul>
    </div>
</div>
<div id="footer-mobile">
    <div class="footer-menu-mobile">
        <h3>Cibaria Stores</h3>
        <div class="footer-menu-mobile-nav">
            <li><a href="http://cibariasoapsupply.com/shop" title="Wholesale Soap Making Oils">Wholesale Soap Making Oils</a></li>
            <li><a href="http://cibaria-intl.com" title="Wholesale Olive Oils">Wholesale Olive Oils</a></li>
        </div>
        <h3>My Account</h3>
        <div class="footer-menu-mobile-nav">
            <li><a href="<?php echo Mage::getBaseUrl() . "customer/account/"; ?>" rel="nofollow">My Account</a></li>
            <li><a href="<?php echo Mage::getBaseUrl() . "sales/order/history/"; ?>" rel="nofollow">Order History</a></li>
        </div>
        <h3>Customer Service</h3>
        <div class="footer-menu-mobile-nav">
            <li><a href="<?php echo Mage::getBaseUrl() . "contact/"; ?>" rel="nofollow">Contact Us</a></li>
            <li><a href="<?php echo Mage::getBaseUrl() . "credit-card-payments"; ?>" rel="nofollow">Credit Card Payments</a></li>
            <li><a href="<?php echo Mage::getBaseUrl() . "freight-policy"; ?>" rel="nofollow">Freight Policy</a></li>
            <li><a href="<?php echo Mage::getBaseUrl() . "payments"; ?>" rel="nofollow">Website Payment Procedure</a></li>
            <li><a href="<?php echo Mage::getBaseUrl() . "300-minimum"; ?>" rel="nofollow">Minimum Order</a></li>
            <li><a href="<?php echo Mage::getBaseUrl() . "ordering-policy"; ?>" rel="nofollow">Ordering Policy</a></li>
            <li><a href="<?php echo Mage::getBaseUrl() . "damage-policy"; ?>" rel="nofollow">Damage Policy</a></li>
            <li><a href="<?php echo Mage::getBaseUrl() . "policy-changes"; ?>" rel="nofollow">Policy Changes</a></li>
            <li><a href="<?php echo Mage::getBaseUrl() . "privacy-policy"; ?>" rel="nofollow">Privacy Policy</a></li>
            <li><a href="<?php echo Mage::getBaseUrl() . "site-terms"; ?>" rel="nofollow">Terms & Conditions</a></li>
        </div>
        <h3>Information</h3>
        <div class="footer-menu-mobile-nav">
            <li><a href="<?php echo Mage::getBaseUrl() . "about-us"; ?>" title="About Us">About Us</a></li>
            <li><a href="<?php echo Mage::getBaseUrl() . "faq"; ?>" title="FAQ">FAQ</a></li>
            <li><a href="<?php echo Mage::getBaseUrl() . "company-holidays"; ?>" title="Company Holidays">Company Holidays</a></li>
            <li><a href="<?php echo Mage::getBaseUrl() . "articles"; ?>" title="Articles">Articles</a></li>
        </div>
    </div>
</div>