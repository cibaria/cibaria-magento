<h1 style="background-color:#99709c; color:white; border-top:1px dotted #ccc; border-bottom: 1px dotted #ccc; padding-left:15px;padding-top:5px;padding-bottom:5px; text-align: center; ">s.a.l.t sister's Spices Top Sellers</h1>

<div class="box">
    <div class="box-content">
        <div class="box-product">
            <?php
            $featured = Mage::getModel('catalog/category')->load(271); // YOU NEED TO CHANGE 5 TO THE ID OF YOUR CATEGORY
            $featured = $featured->getProductCollection();
            foreach ($featured->getAllIds() as $featured_product) {
                $_product = Mage::getModel('catalog/product')->load($featured_product);?>

                <?php

                try {
                    $image_url = $_product->getSmallImage();
                } catch (Exception $e) {
                    $image_url = Mage::getDesign()->getSkinUrl('images/catalog/product/placeholder/image.jpg', array('_area' => 'frontend'));
                }

                ?>

                <div>
                    <!-- Begin boxgrid //-->
                    <div class="boxgrid">
                        <div class="image"><a href="<?php echo $_product->getProductUrl();?>" title="<?php echo $_product->getName();?>"><img src="<?php echo "http://cibariasoapsupply.com/shop/media/catalog/product" . $image_url;?>" alt="<?php echo $_product->getName();?>" height="140px"/></a></div>
                    </div>
                    <!-- End boxgrid //-->

                    <!-- Begin boxgrid bottom //-->
                    <div class="boxgrid-bottom">
                        <div class="name"><a href="<?php echo $_product->getProductUrl();?>" title="<?php echo $_product->getName();?>"><?php echo $_product->getName();?></a></div>
                        <hr/>
                        <a href="<?php echo $_product->getProductUrl();?>" title="<?php echo $_product->getName();?>"><img src="http://www.cibariastoresupply.com/images/more.png" alt="<?php echo $_product->getName();?>" style="padding-top:5px;"></a>

                    </div>
                    <!-- End boxgrid bottom //-->

                </div>
            <?php }?>

        </div>
    </div>
</div>

