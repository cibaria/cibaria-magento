<?php
$functions_path = $_SERVER['DOCUMENT_ROOT'];
$functions_path .= '/includes/functions.php';
require($functions_path);
?>

<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
    <?php include(url_path('/includes/head.php')); ?>
</head>
<body>

<script type="text/javascript">
    jQuery.noConflict();
    var $j = jQuery;
</script>

<?php include(url_path('/includes/header.php')); ?>

<div id="container-wrapper">
    <div id="notification">
    </div>
    <div id="container">
        <div class="breadcrumb">
            &raquo; <a href="<?php echo Mage::getBaseUrl(); ?>" title="Go to Home Page">Home</a>
            &raquo; <strong>Top Sellers</strong>
        </div>
        <div class="category-top-content">
            <h1>Top Sellers</h1>
        </div>
        <?php include(url_path('/includes/left-column.php')); ?>
        <div id="content">
            <?php include(url_path('/includes/content-main-top.php')); ?>
            <?php include(url_path('/includes/products/extra-virgin.php')); ?>
            <?php include(url_path('/includes/products/infused.php')); ?>
            <?php include(url_path('/includes/products/fused.php')); ?>
            <?php include(url_path('/includes/products/flavored-evoo.php')); ?>
            <?php include(url_path('/includes/products/vinegar.php')); ?>
            <?php include(url_path('/includes/products/flavored-balsamic-vinegar.php')); ?>
            <?php include(url_path('/includes/products/flavored-white-balsamic-vinegar.php')); ?>
            <?php include(url_path('/includes/products/tapenades-spreads-sauces.php')); ?>
            <?php include(url_path('/includes/products/olives.php')); ?>
        </div>
        <?php include(url_path('/includes/footer.php')); ?>




