<?php
$functions_path = $_SERVER['DOCUMENT_ROOT'];
$functions_path .= '/shop/includes/functions.php';
require($functions_path);
?>

<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
    <meta charset="UTF-8"/>
    <title>Olive Oil & Vinegar Pairing Guide</title>
    <?php include(url_path('/shop/includes/head.php')); ?>
</head>
<body>
<?php include(url_path('/shop/includes/header.php')); ?>
<div id="container-wrapper">
    <div id="container">
        <div id="content">
            <h1 style="text-align: center;">Oil & Vinegar Pairing Guide</h1>
            <br/>

            <p style="text-align: center;">
                <a href="pairing_guide.pdf">
                    <img src="<?php echo Mage::getBaseUrl(); ?>images/pairing-guide.png" alt="Pairing Guide"/>
                </a>
            </p>
            <br/>

            <p style="text-align:center;">
                <a href="<?php echo Mage::getBaseUrl(); ?>images/pairing_guide_main.pdf"
                   download="pairing_guide_main.pdf">
                    <img src="<?php echo Mage::getBaseUrl(); ?>images/download-new.jpg" alt="Download"/>
                </a>
            </p>
            <?php include(url_path('/shop/includes/footer.php')); ?>
        </div>
    </div>
</div>