<?php
$functions_path = $_SERVER['DOCUMENT_ROOT'];
$functions_path .= '/shop/includes/functions.php';
require($functions_path);
?>
    <!DOCTYPE html>
    <html dir="ltr" lang="en">
    <head>
        <meta charset="UTF-8"/>
        <title>Marketing</title>
        <?php include(url_path('/shop/includes/head.php')); ?>
    </head>

    <body>
    <?php include(url_path('/shop/includes/header.php')); ?>
    <div id="container-wrapper">
        <div id="container">
            <div id="content">
                <h1 style="font-size:30px; text-align:center;padding-top:10px;">Marketing Materials</h1>
                <p>Cibaria Store Supply is happy to bring you customized marketing and POS materials that are specially
                    tailored to your olive oil store and it's sales goals. These free downloads are completely FREE to
                    our customers. While many olive oil stores rely simply on people passing by their store, or
                    word-of-mouth for new customers , we help you get the word out about your business with
                    easy-to-customize marketing tactics, materials, and creative ideas.</p>
                <h2>Not A Marketing Guru?</h2>
                <p>No worries! We make it as simple as possible to utilize our pre-made marketing materials to organize
                    tasting events, special marketing campaigns, and even product features within your store, and
                    online. Keeping your customers informed and engaged is our goal - because we want you to
                    succeed!</p>
                <br/>
                <div style="border: 1px solid #a2a235; background-color: #a3a236; margin-top:18px; ">
                    <h2 style="color:white; text-align:center;padding-top:2px; text-decoration:none;">Marketing Resources</h2>
                </div>
                <div class="listing-type-list catalog-listing">
                    <div class="listing-item">
                        <br/>
                        <div class="box-product">
                            <div>
                                <div class="boxgrid">
                                    <div class="image"><br/><br/><br/>
                                        <img src="<?php echo Mage::getBaseUrl(); ?>images/marketing/trifold.jpg" alt="How To Properly Taste Olive Oil">
                                    </div>
                                </div>
                                <div class="boxgrid-bottom"><div class="name">
                                        <strong>How To Properly Taste Olive Oil<br/>Trifold Flier </strong>
                                    </div>
                                    <div class="price"><a href="<?php echo Mage::getBaseUrl(); ?>marketing/materials/OliveOilTasting-TriFold.pdf" title="How To Properly Taste Olive Oil Trifold Flier">
                                            <img src="<?php echo Mage::getBaseUrl(); ?>images/marketing/download.jpg" alt="download Trifold"/></a>
                                    </div>
                                    <br>
                                </div>
                            </div>
                            <div>
                                <div class="boxgrid">
                                    <div class="image"><br/><br/><br/>
                                        <img src="<?php echo Mage::getBaseUrl(); ?>images/marketing/butter-card.jpg" alt="Olive Oil/Butter Replacement Card"></div>
                                </div>
                                <div class="boxgrid-bottom">
                                    <div class="name"><strong>Olive Oil/Butter<br/>Replacement Card</strong>
                                    </div>
                                    <div class="price">
                                        <a href="<?php echo Mage::getBaseUrl(); ?>marketing/materials/Butter-Replacement-Card.pdf" title="Olive Oil/Butter Replacement Card">
                                            <img src="<?php echo Mage::getBaseUrl(); ?>images/marketing/download.jpg" alt="Download"/></a>
                                    </div>
                                    <br>
                                </div>
                            </div>
                            <div>
                                <div class="boxgrid">
                                    <div class="image"><br/><br/><br/>
                                        <img src="<?php echo Mage::getBaseUrl(); ?>images/marketing/balsamic-vinegars.jpg" alt="Descriptions of Fine Balsamic Vinegars" />
                                    </div>
                                </div>
                                <div class="boxgrid-bottom">
                                    <div class="name"><strong>Descriptions of Fine Balsamic Vinegars
                                        </strong></div>
                                    <div class="price">
                                        <a href="<?php echo Mage::getBaseUrl(); ?>marketing/materials/BALSAMIC-VINEGARS-SHEET-PDF.pdf" title="Descriptions of Fine Balsamic Vinegars">
                                            <img src="<?php echo Mage::getBaseUrl(); ?>images/marketing/download.jpg" alt="Download"/></a></div>
                                    <br>
                                </div>
                            </div>
                            <div>
                                <div class="boxgrid">
                                    <div class="image"><br/><br/><br/>
                                        <img src="<?php echo Mage::getBaseUrl(); ?>images/marketing/recipe-cards.jpg" alt="Recipe Cards"/>
                                    </div>
                                </div>
                                <div class="boxgrid-bottom">
                                    <div class="name"><strong>Recipe Cards</strong></div>
                                    <div class="price"><a
                                            href="<?php echo Mage::getBaseUrl(); ?>recipes/" title="Recipe Cards">
                                            <img src="<?php echo Mage::getBaseUrl(); ?>images/marketing/download.jpg" alt="Recipe Cards"/></a></div>
                                    <br>
                                </div>
                            </div>
                            <div>
                                <div class="boxgrid">
                                    <div class="image"><br/><br/><br/>
                                        <img src="<?php echo Mage::getBaseUrl(); ?>images/marketing/score2.jpg" alt="Small Business Marketing Plan"/>
                                    </div>
                                </div>
                                <div class="boxgrid-bottom">
                                    <div class="name"><strong>Small Business Marketing Plan</strong></div>
                                    <div class="price"><a href="<?php echo Mage::getBaseUrl(); ?>images/marketing/Small Business Marketing Plan.docx" title="Small Business Marketing Plan">
                                            <img src="<?php echo Mage::getBaseUrl(); ?>images/marketing/download.jpg" alt="Small Business Marketing Plan"/></a></div>
                                    <br>
                                </div>
                            </div>
                        </div>
                        <br/>
                    </div>
                </div>
                <div style="border: 1px solid #a2a235; background-color: #a3a236; margin-top:18px; ">
                    <h2 style="color:white; text-align:center;padding-top:2px; text-decoration:none;">Hidden Sales Opportunities</h2>
                </div>
                <br/>
                <p>We've compiled some FREE must-have reports, especially for Olive Oil Store owners and Specialty Food
                    Store owners. These priceless reports are available to Cibaria Store Supply customers only, and will
                    help you make the most out of your marketing budget by including low-cost or no-cost olive oil store
                    marketing ideas and fresh ways to keep your olive oil store customers engaged and interested in your
                    store that has its brand and focus.</p>
                <p>Have you ever asked yourself what you could do to capitalize on your current customer base? Did you
                    ever feel that you were missing out on possible sales? Perhaps it's because of small things, or
                    large things - there are definitely more sales to be had. By clarifying your ultimate goals, your
                    business will thrive when you realize all of the hidden sales opprotunities! Whether you are just
                    starting your first store, or if you're looking for ways to elicit growth in your store, we've got
                    great sales opportunities for everyone.</p>
                <div class="listing-type-list catalog-listing">
                    <div class="listing-item">
                        <br/>
                        <div class="box-product">
                            <div>
                                <div class="boxgrid">
                                    <div class="image"><br/><br/><br/>
                                        <img src="<?php echo Mage::getBaseUrl(); ?>images/marketing/1.jpg" alt="How to Create Engagement In Your Olive Oil Store">
                                    </div>
                                </div>
                                <div class="boxgrid-bottom">
                                    <div class="name"><strong>How to Create Engagement In Your Olive Oil Store</strong></div>
                                    <div class="price">
                                        <a href="<?php echo Mage::getBaseUrl(); ?>marketing/materials/Vol1-HiddenSalesOpportunities.pdf" title="How to Create Engagement In Your Olive Oil Store">
                                            <img src="<?php echo Mage::getBaseUrl(); ?>images/marketing/download.jpg" alt="Download"/></a>
                                    </div>
                                    <br>
                                </div>
                            </div>
                            <div>
                                <div class="boxgrid">
                                    <div class="image"><br/><br/><br/>
                                        <img src="<?php echo Mage::getBaseUrl(); ?>images/marketing/2.jpg" alt="The Shocking Sales Impact of an Email Address">
                                    </div>
                                </div>
                                <div class="boxgrid-bottom">
                                    <div class="name"><strong>The Shocking Sales Impact of an Email Address</strong></div>
                                    <div class="price"><a href="<?php echo Mage::getBaseUrl(); ?>marketing/materials/Vol2-HiddenSalesOpportunities.pdf" title="The Shocking Sales Impact of an Email Address">
                                            <img src="<?php echo Mage::getBaseUrl(); ?>images/marketing/download.jpg" alt="Download"/></a></div>
                                    <br>
                                </div>
                            </div>
                            <div>
                                <div class="boxgrid">
                                    <div class="image"><br/><br/><br/>
                                        <img src="<?php echo Mage::getBaseUrl(); ?>images/marketing/3.jpg" alt="The Secrets of Successful Direct Mail Marketing"/>
                                    </div>
                                </div>
                                <div class="boxgrid-bottom">
                                    <div class="name"><strong>The Secrets of Successful Direct Mail Marketing</strong></div>
                                    <div class="price"><a href="<?php echo Mage::getBaseUrl(); ?>marketing/materials/Vol3-HiddenSalesOpportunities.pdf" title="The Secrets of Successful Direct Mail Marketing">
                                            <img src="<?php echo Mage::getBaseUrl(); ?>images/marketing/download.jpg" alt="Download"/></a></div>
                                    <br>
                                </div>
                            </div>
                            <div>
                                <div class="boxgrid">
                                    <div class="image"><br/><br/><br/>
                                        <img src="<?php echo Mage::getBaseUrl(); ?>images/marketing/4.jpg" alt="How to Get Publicity for your Olive Oil Store"/>
                                    </div>
                                </div>
                                <div class="boxgrid-bottom">
                                    <div class="name"><strong>How to Get Publicity for your Olive Oil Store</strong></div>
                                    <div class="price"><a href="<?php echo Mage::getBaseUrl(); ?>marketing/materials/Vol4-HiddenSalesOpportunities.pdf" title="How to Get Publicity for your Olive Oil Store">
                                            <img src="<?php echo Mage::getBaseUrl(); ?>images/marketing/download.jpg" alt="Download"/></a></div>
                                    <br>
                                </div>
                            </div>
                            <div>
                                <div class="boxgrid">
                                    <div class="image"><br/><br/><br/>
                                        <img src="<?php echo Mage::getBaseUrl(); ?>images/marketing/5.jpg" alt="Bring More People Into Your Store Using Location-Based Social Websites" />
                                    </div>

                                </div>
                                <div class="boxgrid-bottom">
                                    <div class="name"><strong>Bring More People Into Your Store Using Location-Based Social Websites
                                        </strong></div>
                                    <div class="price"><a href="<?php echo Mage::getBaseUrl(); ?>marketing/materials/Vol5-HiddenSalesOpportunities.pdf" title="Bring More People Into Your Store Using Location-Based Social Websites">
                                            <img src="<?php echo Mage::getBaseUrl(); ?>images/marketing/download.jpg" alt="Download"/></a></div>
                                    <br>
                                </div>
                            </div>
                            <div>
                                <div class="boxgrid">
                                    <div class="image"><br/><br/><br/>
                                        <img src="<?php echo Mage::getBaseUrl(); ?>images/marketing/6.jpg" alt="Groupon - Brings Immediate and Very Large Results To Your Olive Oil Store"/>
                                    </div>
                                </div>
                                <div class="boxgrid-bottom">
                                    <div class="name"><strong>Groupon - Brings Immediate and Very Large Results To Your Olive Oil Store</strong></div>
                                    <div class="price"><a href="<?php echo Mage::getBaseUrl(); ?>marketing/materials/Vol6-HiddenSalesOpportunities.pdf" title="Groupon - Brings Immediate and Very Large Results To Your Olive Oil Store">
                                            <img src="<?php echo Mage::getBaseUrl(); ?>images/marketing/download.jpg" alt="Groupon - Brings Immediate and Very Large Results To Your Olive Oil Store"/></a></div>
                                    <br>
                                </div>
                            </div>
                        </div>
                        <br/>
                        <div class="box-product">
                            <div>
                                <div class="boxgrid">
                                    <div class="image"><br/><br/><br/><img src="<?php echo Mage::getBaseUrl(); ?>images/marketing/7.jpg" alt="11 Reasons Why You Need a Great Website!" />
                                    </div>
                                </div>
                                <div class="boxgrid-bottom">
                                    <div class="name"><strong>11 Reasons Why You Need a Great Website!</strong></div>
                                    <div class="price"><a href="<?php echo Mage::getBaseUrl(); ?>marketing/materials/Vol7-HiddenSalesOpportunities.pdf" title="11 Reasons Why You Need a Great Website">
                                            <img src="<?php echo Mage::getBaseUrl(); ?>images/marketing/download.jpg" alt="11 Reasons Why You Need a Great Website"/></a></div>
                                    <br>
                                </div>
                            </div>
                            <div>
                                <div class="boxgrid">
                                    <div class="image"><br/><br/><br/>
                                        <img src="<?php echo Mage::getBaseUrl(); ?>images/marketing/8.jpg" alt="The Secrets of Stellar Customer Service to Create a World-Class Olive Oil Store"/>
                                    </div>
                                </div>
                                <div class="boxgrid-bottom">
                                    <div class="name"><strong>The Secrets of Stellar Customer Service to Create a World-Class Olive Oil Store</strong></div>

                                    <div class="price"><a href="<?php echo Mage::getBaseUrl(); ?>marketing/materials/vol8-hiddensalesopportunities.pdf" title="The Secrets of Stellar Customer Service to Create a World-Class Olive Oil Store">
                                            <img src="<?php echo Mage::getBaseUrl(); ?>images/marketing/download.jpg" alt="Download"/></a></div>
                                    <br>
                                </div>
                            </div>
                            <div>
                                <div class="boxgrid">
                                    <div class="image"><br/><br/><br/>
                                        <img src="<?php echo Mage::getBaseUrl(); ?>images/marketing/9.jpg" alt="The Secrets of a Killer Olive Oil Store Brand"/>
                                    </div>
                                </div>
                                <div class="boxgrid-bottom">
                                    <div class="name"><strong>The Secrets of a Killer Olive Oil Store Brand</strong></div>
                                    <div class="price"><a href="<?php echo Mage::getBaseUrl(); ?>marketing/materials/Vol9-HiddenSalesOpportunities.pdf" title="The Secrets of a Killer Olive Oil Store Brand">
                                            <img src="<?php echo Mage::getBaseUrl(); ?>images/marketing/download.jpg" alt="Download"/></a></div>
                                    <br>
                                </div>
                            </div>
                        </div>
                        <div class="box-product">
                            <div>
                                <div class="boxgrid">
                                    <div class="image"><br/><br/><br/>
                                        <img src="<?php echo Mage::getBaseUrl(); ?>images/marketing/10.jpg" alt="How to Hold Successful Olive Oil Store Events" />
                                    </div>
                                </div>
                                <div class="boxgrid-bottom">
                                    <div class="name"><strong>How to Hold Successful Olive Oil Store Events</strong></div>
                                    <div class="price">
                                        <a href="<?php echo Mage::getBaseUrl(); ?>marketing/materials/Vol10-HiddenSalesOpportunities.pdf" title="How to Hold Successful Olive Oil Store Events">
                                            <img src="<?php echo Mage::getBaseUrl(); ?>images/marketing/download.jpg" alt="Download"/></a></div>
                                    <br>
                                </div>
                            </div>
                            <div>
                                <div class="boxgrid">
                                    <div class="image"><br/><br/><br/>
                                        <img src="<?php echo Mage::getBaseUrl(); ?>images/marketing/11.jpg" alt="The Secrets to Customer Service that Creates Evangelists For your Store"/>
                                    </div>
                                </div>
                                <div class="boxgrid-bottom">
                                    <div class="name"><strong>The Secrets to Customer Service that Creates Evangelists For your Store!</strong></div>
                                    <div class="price"><a
                                            href="<?php echo Mage::getBaseUrl(); ?>marketing/materials/Vol11-HiddenSalesOpportunities.pdf" title="The Secrets to Customer Service that Creates Evangelists For your Store">
                                            <img src="<?php echo Mage::getBaseUrl(); ?>images/marketing/download.jpg" alt="Download"/></a></div>
                                    <br>
                                </div>
                            </div>
                            <div>
                                <div class="boxgrid">
                                    <div class="image"><br/><br/><br/><img src="<?php echo Mage::getBaseUrl(); ?>images/marketing/12.jpg" alt="How to Be Sure Your Olive Oil Store is Ready for the Gift-Giving Season" />
                                    </div>
                                </div>
                                <div class="boxgrid-bottom">
                                    <div class="name"><strong>How to Be Sure Your Olive Oil Store is Ready for the Gift-Giving Season</strong></div>
                                    <div class="price"><a href="<?php echo Mage::getBaseUrl(); ?>marketing/materials/Vol12-HiddenSalesOpportunities.pdf" title="How to Be Sure Your Olive Oil Store is Ready for the Gift-Giving Season">
                                            <img src="<?php echo Mage::getBaseUrl(); ?>images/marketing/download.jpg" alt="Download"/></a></div>
                                    <br>
                                </div>
                            </div>
                        </div>
                        <div class="box-product">
                            <div>
                                <div class="boxgrid">
                                    <div class="image"><br/><br/><br/><img src="<?php echo Mage::getBaseUrl(); ?>images/marketing/13.jpg" alt="How to Create an Engaging Blog For Your Olive Oil Store Website"/>
                                    </div>
                                </div>
                                <div class="boxgrid-bottom">
                                    <div class="name"><strong>How to Create an Engaging Blog For Your Olive Oil Store Website
                                        </strong></div>
                                    <div class="price"><a href="<?php echo Mage::getBaseUrl(); ?>marketing/materials/Vol13-HiddenSalesOpportunities.pdf" title="How to Create an Engaging Blog For Your Olive Oil Store Website">
                                            <img src="<?php echo Mage::getBaseUrl(); ?>images/marketing/download.jpg" alt="Download"/></a></div>
                                    <br>
                                </div>
                            </div>
                            <div>
                                <div class="boxgrid">
                                    <div class="image"><br/><br/><br/>
                                        <img src="<?php echo Mage::getBaseUrl(); ?>images/marketing/14.jpg" alt="Create Gift Packs that Sell"/>
                                    </div>
                                </div>
                                <div class="boxgrid-bottom">
                                    <div class="name"><strong>Create Gift Packs that Sell!</strong>
                                    </div>
                                    <div class="price"><a href="<?php echo Mage::getBaseUrl(); ?>marketing/materials/Vol14-HiddenSalesOpportunities.pdf" title="Create Gift Packs that Sell">
                                            <img src="<?php echo Mage::getBaseUrl(); ?>images/marketing/download.jpg" alt="Download"/></a></div>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php include(url_path('/shop/includes/footer.php')); ?>
            </div>
        </div>
    </div>
    </body>
    </html>