<style type="text/css">

.tabs {

  position: relative;

  min-height: 1080px; /* This part sucks */

  /*clear: both;*/

  margin: 25px 0;

}

.tab {

  float: left;

}

.tab label {

  background: #eee;

  padding: 10px;

  border: 1px solid #ccc;

  margin-left: -1px;

  position: relative;

  left: 1px;

}

.tab [type=radio] {

  display: none;

}

.content-box {

  position: absolute;

  top: 22px;

  left: 0;

  background: white;

  right: 0;

  bottom: 0;

  padding: 20px;

  border: 1px solid #ccc;

}

[type=radio]:checked ~ label {

  background: white;

  border-bottom: 1px solid white;

  z-index: 2;

}

[type=radio]:checked ~ label ~ .content-box {

  z-index: 1;

}

    </style>



<div style=" background-color: #F6F6E4;

            border: 1px solid #E1E1E1;

            border-radius: 5px 5px 5px 5px;

            width: 100%;">



    <div style="padding-left:25px;padding-right:25px;">

        <h1 style="border-bottom: 1px solid #e1e1e1;">Bulletin Board </h1>

        <p>Click on one of the tabs below for more information.</p>

        <!-- tabs -->

       <div class="tabs">


<!-- updates tab -->
<div class="tab">
       <input type="radio" id="tab-1" name="tab-group-1" checked>
       <label for="tab-1">Updates</label>
       <div class="content-box">
<h1>Updates</h1>
<h2><a href="http://www.cibariastoresupply.com/shop/12-375-ml-bordelese-uvag.html">12/375 ML Bordelese (Short) UVAG Bottles are back in Stock</a> Posted: 3/20/2018</h2>
    <div style="border-bottom: 1px solid #e1e1e1;height:2px;"></div>
<h2>Northern Hemisphere New Crop Varietals are in house!</h2>
<ul>
  <li><strong>California Arbequina</strong></li>
  <li><strong>California Arbosana</strong></li>
  <li><strong>California Mission</strong></li>
  <li><strong>California Sevillano</strong></li>
  <li><strong>California Arbequina, Arbosana, Koroneiki</strong></li>
  <li><strong>California Frantoio, Leccino, Pendolino</strong></li>
  <li><strong>California Sevillano, Ascolano</strong></li>
  <li><strong>Greek Koroneiki</strong></li>
  <li><strong>Greek Kalamata</strong></li>
  <li><strong>Spanish Signature</strong></li>
  <li><strong>Spanish Hojiblanca</strong></li>
  <li><strong>Spanish Arbequina</strong></li>
  <li><strong>Spanish Picual</strong></li>
</ul>
       <div style="border-bottom: 1px solid #e1e1e1;height:2px;"></div>      
         <h2>Flavored Base Oil Origin</h2>
        <ul>
            <li>Flavored Extra Virgin Olive Oils: <strong>Tunisian and Spanish Blend</strong></li>
            <li>Infused Oils: <strong>Spain</strong></li>
        </ul>
       </div>
   </div>



<!-- important info tab -->
   <div class="tab">
       <input type="radio" id="tab-2" name="tab-group-1">
       <label for="tab-2">Important Information</label>
       <div class="content-box">

<h1>ATTENTION TO ALL OUR VALUED CUSTOMERS</h1>
<p><strong>To help us serve you better during this extremely heavy volume period the following company policies and procedures are in effect:</strong></p>
Please check the website, <a href="http://www.cibariastoresupply.com">www.cibariastoresupply.com</a> for updates
<p>
  <br>
<strong>1.OUT OF STOCK:</strong> If an item is shown as “Out of Stock”, it is currently unavailable, but we are usually waiting arrival on more.  Please visit the website periodically and click on the "Updates" tab above to see if an item is back in stock.  Unfortunately, we are unable to estimate when an item will be back in stock.  We will post in the "Updates" tab at the top as soon as we have a confirmation.<br><br> 
<strong>2.PRICE FLUCUATIONS:</strong> Cibaria diligently works to control costs and maintain competitive prices.  However, prices are subject to change without notice.  Please be sure to check your Sales Order Confirmation for current cost.<br><br>
<strong>3.EXTENDED LEAD TIME:</strong> During peak seasonal times our reply/response time and production lead times may be increased.  We appreciate your patience during this time and ask that you plan to order earlier than usual so it arrives on schedule.<br><br>
<strong>4.ORDERING POLICIES:</strong> Additional items or count increases will be handled as a new Sales Order.  It is the customer’s choice whether the original order is held to ship with the new order.  Deletion of an item or decreased quantities will result in a 15% re-stocking fee.  Private Label products will be charged 100% since these are customized products.<br><br>
<strong>5.FREIGHT HANDLING:</strong> Small orders are usually shipped via UPS unless customer request otherwise.  Large orders are shipped on a pallet to reduce shipment costs.  Any special shipping or delivery requirements may incur additional cost.  Shipping estimates (when requested) may be given once the order is processed however, exact shipment quotes will not be available until the full order is packed and weighed for shipping.<br><br>
<strong>6.PRODUCT CONSISTENCY:</strong> Natural products are always in a state of change since they are not mechanically or chemically processed.  Changes may be in color, flavor intensity, viscosity, potency, sediment, etc.  Cibaria uses the 25 Star Balsamic Vinegar from Modena Italy as the base for all the vinegar flavors we create.  The viscosity (thickness) and flavor intensity will vary from batch to batch, lot to lot, season to season.  We have an acceptable range that we test against and although we cannot always recreate exactly the same, we stay within the acceptable range.  The same is true for the flavored extra virgin olive oils.  
</p>
       </div>
   </div>

   
   <!-- First Info tab -->
   <div class="tab">
       <input type="radio" id="tab-3" name="tab-group-1">
       <label for="tab-3">First Order Info</label>
       <div class="content-box">
        <h1>First Order Info</h1>
          <h2>First Order With Us?</h2>
        <a href="http://www.cibariastoresupply.com/shop/first-order-with-cibaria">Click Here</a> for more information.
		<div style="border-bottom: 1px solid #e1e1e1;height:2px;"></div>
        <h2>How to order from our website</h2>
        <a href="http://www.cibariastoresupply.com/shop/how-to-order">Click Here</a> for more information.
		<div style="border-bottom: 1px solid #e1e1e1;height:2px;"></div>
		<h2>Minimum Order</h2>
		<a href="http://www.cibariastoresupply.com/shop/300-minimum">Click Here</a> for more information.
		<div style="border-bottom: 1px solid #e1e1e1;height:2px;"></div>
		<h2>Website Payment Procedure</h2>
		<a href="http://www.cibariastoresupply.com/shop/payments">Click Here</a> for more information.
		<div style="border-bottom: 1px solid #e1e1e1;height:2px;"></div>
        <h2>Credit Card Payments</h2>
        <a href="http://www.cibariastoresupply.com/shop/credit-card-payments/">Click Here</a> form more information.
		<div style="border-bottom: 1px solid #e1e1e1;height:2px;"></div>
        <h2>Freight Policy</h2>
        <a href="http://www.cibariastoresupply.com/shop/freight-policy">Click Here</a>  form more information.
       </div>
   </div>

   <!-- Education tab -->
<div class="tab">
       <input type="radio" id="tab-4" name="tab-group-1">
       <label for="tab-4">Education</label>
       <div class="content-box">
        <h1>Education</h1>
        <h2>Glossary of olive oil terminology</h2>
        <a href="http://blog.aboutoliveoil.org/glossary-of-olive-oil-terminology-0?">Click Here</a>
<div style="border-bottom: 1px solid #e1e1e1;height:2px;"></div>
        <h2>Olive Oil Biscotti</h2>
        <a href="http://blog.aboutoliveoil.org/olive-oil-biscotti">Click Here</a>
<div style="border-bottom: 1px solid #e1e1e1;height:2px;"></div>
        <h2>Roasting Whole Cauliflower </h2>
        <a href="http://blog.aboutoliveoil.org/roasting-whole-cauliflower">Click Here</a>
<div style="border-bottom: 1px solid #e1e1e1;height:2px;"></div>

        <h2>UC Davis</h2>
        <a href="http://olivecenter.ucdavis.edu/front-page">Click Here</a>
<div style="border-bottom: 1px solid #e1e1e1;height:2px;"></div>
        <h2>Olive Oil Classes</h2>
        <a href="https://www.oliveoilschool.org/">Click Here</a>
<div style="border-bottom: 1px solid #e1e1e1;height:2px;"></div>

<h1>Archive</h1>
  <h2>North American Olive Oil Association links and videos. <br />
<a href="https://livingnongmo.org/2016/12/22/why-the-non-gmo-project-verifies-low-risk-ingredients/">https://livingnongmo.org/2016/12/22/why-the-non-gmo-project-verifies-low-risk-ingredients/</a><br />
<a href="http://www.aboutoliveoil.org/resources/videos.html">http://www.aboutoliveoil.org/resources/videos.html</a><br />
<a href="http://www.aboutoliveoil.org/cookingwitholiveoil.pdf">http://www.aboutoliveoil.org/cookingwitholiveoil.pdf</a><br />
<a href="http://www.aboutoliveoil.org/download_ec_cooking.pdf">http://www.aboutoliveoil.org/download_ec_cooking.pdf</a><br />
</h2>
       </div>
   </div>
</div>

<!-- end tabs -->

        <?php echo $this->getChildHtml('global_notices')?>

    </div>

</div>
