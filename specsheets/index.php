<?php
$functions_path = $_SERVER['DOCUMENT_ROOT'];
$functions_path .= '/shop/includes/functions.php';

require($functions_path);
?>
    <?php if ($session->isLoggedIn()) { ?>
        <html dir="ltr" lang="en">
        <head>
            <meta charset="UTF-8"/>
            <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Specsheets</title>
        <?php include(url_path('/shop/includes/head.php')); ?>
        <link rel="stylesheet" type="text/css" href="<?php echo Mage::getBaseUrl(); ?>css/jquery.dataTables.min.css" />
    </head>

    <body>
        <?php include(url_path('/shop/includes/google-analytics.php')); ?>
        <script type="text/javascript">
        jQuery.noConflict();
        var $j = jQuery;
        </script>
        <?php include(url_path('/shop/includes/header.php')); ?>
            <div id="container-wrapper">
                <div id="container">
                    <div id="content">
                        <h1 style="text-align: center;">Specsheets</h1>
                        <br />
                        <table class="attribute" id="specsheets">
                            <thead>
                                <tr>
                                    <td>Title</td>
                                    <td>Description</td>
                                    <td>&nbsp;</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($specsheets as $result) { ?>
                                <?php if ($result['isdone'] === 'Yes') { ?>
                                <?php if($result['website'] == 'both' || $result['website'] == 'store supply' || $result['website'] != 'none'){ ?>
                                <?php if ($result['website'] != 'soap supply') { ?>
                                <tr>
                                    <?php include(url_path('/shop/includes/_form.php')); ?>
                                    <td><strong><?php echo $result['title'];?></strong></td>
                                    <td><?php echo substr($result['description'], 0, 150) . '...';?></td>
                                    <td valign="center"><input type="image" src="<?php echo Mage::getBaseUrl() ?>images/specsheets/download.jpg" alt="Download" /></td>
                                    </form>
                                </tr>
                                <?php } ?>
                                <?php }}} ?>
                            </tbody>
                        </table>

                        <?php include(url_path('/shop/includes/footer.php')); ?>
                        <script type="text/javascript" src="<?php echo Mage::getBaseUrl(); ?>js/jquery.dataTables.min.js"></script>
                        <script type="text/javascript">
                        $j(document).ready(function() {
                            $j('#specsheets').DataTable();
                        });
                        </script>
                    </div>
                </div>
            </div>
    </body>
    </html>
    <?php } else {  echo "<script type='text/javascript'>window.location.assign('/shop/customer/account/login/')</script>"; } ?>
    